# HbtzManager Demo #


### What is this repository for? ###

    The HbtzManager Source contains the code used to implement the example project used to demonstrate the implementation of the XCFramework for the Heartbeatz implementation. This is a submodule to be used to create the example project and it is used in the project for developing the XCFramework.

* Version: 1.0.0

### How do I get set up? ###

* Summary of set up

Clone the demo source code repository
Clone the HbtzManager XCFramework

* Configuration

Create a Config.xcconfig file to provide your Integrator Key in the following format

    MY_SECRET_API_KEY = EnterIntegratorKeyHere
    

* Dependencies

The example implementation uses the HbtzManager XCFramework to access the different flavors of the Heartbeatz enabled devices.  The HbtzManager XCFramework neeeds to be available to the project.

* How to run tests

The demo application provides enable switches for each type of Heartbeatz device.  This allows the user to test each type of device independantly or together.

The user needs to ensure the HealthKit and Bluetooth permissions are enabled for the app to function properly.  The Bluetooth permissions are needed to ensure the CoreBluetooth resources are accessible.  The HealthKit permissions are needed to provide heart rate values and allow the app the ability to run in the background while sending data via the Bluetooth interface.

> <key>NSHealthShareUsageDescription</key>  
> <string>$(PRODUCT_NAME) would like to access your heart rate for the purpose of using the data to send data to Heartbeatz devices. Please grant this permission to continue.</string>  
> <key>NSHealthUpdateUsageDescription</key>  
> <string>$(PRODUCT_NAME) would like to update your health usage with other fitness metrics.</string>  
> <key>NSBluetoothPeripheralUsageDescription</key>  
> <string>$(PRODUCT_NAME) would like to access Bluetooth for communicating with Heartbeatz devices. Please grant this permission to continue.</string>  
> <key>NSBluetoothAlwaysUsageDescription</key>  
> <string>$(PRODUCT_NAME) would like to access Bluetooth for communicating with Heartbeatz devices. Please grant this permission to continue.</string>

#### Personal Devices ####

The personal devices such as Watch Link/USB, Runn, or WYÛR are devices a users would like to pair with once then have available any time the device is available.  In these cases an example is provided for how the application can connect to the device when the deviceShouldConnect delegate fires and the RSSI for the device is > -50db.  When the deviceDidConnect fires, the serialNumber of the device is retained so the appplication can instruct the Framework to connect immediately when the device is seen again irrespective of the RSSI.

#### Group Devices ####

The Group type devices should allow the device to connect as soon as it is available to the watch.  This type of device should not be retained for reconnection. 

#### Shared Devices ####

The Shared type devices should allow the device to connect when the watch is in close proximity of the device and the device should only be retained for the duration of the workout. This allows the watch to automatically reconnect if the link is dropped for any reason. 

* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin:
   
>   Rick Gibbs
    North Pole Engineering
    rickg@npe-inc.com 
   
* Other community or team contact

>   Nick Moen nickm@npe-inc.com
