//
//  AppDelegate.swift
//  DemoApp WatchKit Extension
//
//  Created by Rick Gibbs on 12/20/21.
//

#if os(watchOS)
import WatchKit
#endif
import HbtzManager
import HealthKit
import CoreBluetooth
import UIKit
import SwiftUI
import UserNotifications

public enum HapticControl {
    case none
    case warning
    case all
}

public enum AntDeviceType: UInt8 {
    case none = 0
    case punchSensor = 0x06
    case bikepower = 0x0B
    case fitnessEquipment = 0x11
    case activitymonitor = 0x15
    case test = 0x77
    case heartrate = 0x78
    case bikespeedcadence = 0x79
    case bkecadence = 0x7A
    case bikespeed = 0x7B
    case footpod = 0x7C
    case other = 0xff
    
    var description : String
    {
        switch self {
        case .none: return "None"
        case .punchSensor: return "Punch Sensor"
        case .bikepower: return "Bike Power"
        case .fitnessEquipment: return "Fitness Equipment"
        case .activitymonitor: return "Activity Monitor"
        case .test: return "Test"
        case .heartrate: return "Heart Rate"
        case .bikespeedcadence: return "Bike S/C"
        case .bkecadence: return "Bike Cad"
        case .bikespeed: return "Bike Speed"
        case .footpod: return "Footpod"
        case .other: return "Other"
        }
    }
}

public enum HbtzCommand: UInt8 {
    case heartRateData = 0x42
    case deviceAsciiName = 0x49
    
    var description : String
    {
        switch self {
        case .heartRateData: return "Heart Rate Data"
        case.deviceAsciiName: return "Device ASCII Name"
        }
    }
}

public class AntDevice : Hashable, Equatable, Identifiable, Comparable, ObservableObject {
    public static func < (lhs: AntDevice, rhs: AntDevice) -> Bool {
        return lhs.deviceID < rhs.deviceID
    }
    
    public static func == (lhs: AntDevice, rhs: AntDevice) -> Bool {
        return lhs.deviceID == rhs.deviceID && lhs.deviceType == rhs.deviceType
    }
    
    public let deviceID: UInt32
    public let deviceType: AntDeviceType
    public let transType: UInt8
    @Published public var rssi: Int8
    private let initDate: Date
    public var packetsReceived: [Int]
    @Published public var pps: Double
    @Published public var lastSeen: Date
    
    //public var count: Int
    public var data: Data
    {
        didSet {
            //count += 1
            packetsReceived[packetsReceived.count-1] = packetsReceived[packetsReceived.count-1] + 1
            lastSeen = Date()
        }
    }
    
    public var hrValue: UInt8 {
        get {
            let dataArray =  data.bytes
            if dataArray.count >= 8 {
                return dataArray[7]
            }
            return 0
        }
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(deviceID)
    }
    
    public init(deviceID: UInt32, deviceType: AntDeviceType, transType: UInt8, rssi: Int8, data: Data){
        self.deviceID = deviceID
        self.deviceType = deviceType
        self.transType = transType
        self.rssi = rssi
        self.data =  data
        self.packetsReceived  =  [0,0,0]
        self.initDate = Date()
        self.lastSeen = Date()
        self.pps = 0
        //self.count = 0
    }
    
    public func calcPps()
    {
        //print("Packets Received(\(self.deviceType):\(self.deviceID)):\(packetsReceived.last ?? 0)  \(pps)")
       //var index = 1
        pps = Double(packetsReceived.reduce(0, +)) / Double(packetsReceived.count)
        for i in 1 ..< packetsReceived.count {
            packetsReceived[i-1] = packetsReceived[i]
        }
        packetsReceived[packetsReceived.count-1] = 0
    }
}


public class HbtzDevice : Hashable, Equatable, Identifiable, Comparable, ObservableObject {
    public static func < (lhs: HbtzDevice, rhs: HbtzDevice) -> Bool {
        return lhs.deviceID < rhs.deviceID
    }
    
    public static func == (lhs: HbtzDevice, rhs: HbtzDevice) -> Bool {
        return lhs.deviceID == rhs.deviceID
    }
    
    public let deviceID: UInt32
    @Published public var deviceName: String?
    @Published public var rssi: Int8
    private let initDate: Date
    public var packetsReceived: [Int]
    @Published public var pps: Double
    @Published public var lastSeen: Date

    //public var count: Int
    public var data: Data
    {
        didSet {
            if data.count > 1 {
                let array = data.bytes
                switch HbtzCommand(rawValue: array[0]) {
                case .heartRateData:
                    hrValue = array[1]
                    if deviceName == nil {
                        let result = HbtzBleManager.sharedHbtzBleManager.requestDeviceName(serialNumber: self.deviceID)
                        //print("Request Result:\(result)")
                    }
                case .deviceAsciiName: deviceName = data.subdata(in: 1..<data.count).stringUTF8
                default:
                    break
                }
                //count += 1
                packetsReceived[packetsReceived.count-1] = packetsReceived[packetsReceived.count-1] + 1
                lastSeen = Date()
            }
        }
    }
    
    public var hrValue: UInt8?

    public func hash(into hasher: inout Hasher) {
        hasher.combine(deviceID)
    }
    
    public init(deviceID: UInt32, data: Data){
        self.deviceID = deviceID
        self.rssi = -128
        self.data =  data
        self.packetsReceived  =  [0,0,0]
        self.initDate = Date()
        self.lastSeen = Date()
        self.pps = 0
        //self.count = 0
    }
    
    public func calcPps()
    {
        //print("Packets Received(\(self.deviceID)):\(packetsReceived.last ?? 0)  \(pps)")
       //var index = 1
        pps = Double(packetsReceived.reduce(0, +)) / Double(packetsReceived.count)
        for i in 1 ..< packetsReceived.count {
            packetsReceived[i-1] = packetsReceived[i]
        }
        packetsReceived[packetsReceived.count-1] = 0
    }
}


class ManagerDelegate: NSObject, ObservableObject {
    
    public static let sharedManager = ManagerDelegate()
    
    public var selectedType: HeartbeatzProductType?
    public var proximityConnect: Bool = false
    @Published public var managerState: CBManagerState = .unknown
    private let printValueUpdates = false
    @Published public var antDevices = Set<AntDevice>()
    @Published public var hbtzDevices = Set<HbtzDevice>()

    public var ppsTimer: Timer

    let hbtzManager = HbtzBleManager.sharedHbtzBleManager
    #if os(watchOS)
    let hkManager = HkManager.sharedManager
    #endif
    
//    let scanTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { timer in
//        print("scanning2:\(HbtzBleManager.sharedHbtzBleManager.isScanning)")
//    })

    var frameworkVersion:  String  {
        get {
            return hbtzManager.currentFrameworkVersion
        }
    }
    
    //haptic
    public var useHaptic: HapticControl = .all
    var watchFaceDimmedWorkoutActive = true

    @Published public var wlCtrl = DeviceTypeControl(title: "WATCH LINK™", type: .heartbeatz)
    @Published public var hbtzUsbLegacyCtrl = DeviceTypeControl(title: "heartbeatz USB Legacy", type: .heartbeatzHbtzUSB, subType: HeartbeatzUsbOperationalMode.legacy)
    @Published public var hbtzUsbCtrl = DeviceTypeControl(title: "heartbeatz USB", type: .heartbeatzHbtzUSB, subType: HeartbeatzUsbOperationalMode.single)
    @Published public var hbtzHubbCtrl = DeviceTypeControl(title: "heartbeatz Hubb", type: .heartbeatzHbtzUSB, subType: HeartbeatzUsbOperationalMode.hubb)
    @Published public var hbtzHornetCtrl = DeviceTypeControl(title: "heartbeatz Hornet", type: .heartbeatzHbtzUSB, subType: HeartbeatzUsbOperationalMode.hubb, isHornet: true)
    @Published public var groupCtrl = DeviceTypeControl(title: "WASP3 Group", type: .heartbeatzClub)
    @Published public var runnCtrl = DeviceTypeControl(title: "Runn", type: .heartbeatzRunn)
    @Published public var wyûrCtrl = DeviceTypeControl(title: "WYÛR", type: .heartbeatzWyûr)
    @Published public var côrdCtrl = DeviceTypeControl(title: "CÔRD", type: .heartbeatzWyûrUSB)
    @Published public var gemCtrl = DeviceTypeControl(title: "GEM", type: .heartbeatzGem)
    @Published public var broadcastCtrl = DeviceTypeControl(title: "WASP Broadcast", type: .heartbeatzBcast)
    @Published var controllers = [DeviceTypeControl]()

#if os(iOS)
    @Published var currentHeartRate:Measurement<UnitCadence> = Measurement(value: Double.random(in: 65.0..<158.0).rounded(), unit: .beatsPerMinute)
    @Published var activeCalories:Measurement<UnitEnergy> = Measurement(value: 45.0, unit: .kilocalories)
    @Published var totalCalories:Measurement<UnitEnergy> = Measurement(value: 60.0, unit: .kilocalories)
    #else
    @Published var currentHeartRate:Measurement<UnitCadence> = Measurement(value: 0.0, unit: .beatsPerMinute)
    @Published var activeCalories:Measurement<UnitEnergy> = Measurement(value: 0.0, unit: .kilocalories)
    @Published var totalCalories:Measurement<UnitEnergy> = Measurement(value: 0.0, unit: .kilocalories)
    #endif

    @Published var localAntId: UInt32 = UInt32.random(in: 1..<1048575)
    @Published var isHbtzEnabled: Bool = false
    
    @Published var strongestRssi: BleDiscoveryDevice?
    @Published var connectingController: DeviceTypeControl?
    
    //let testDevices = [TestDevice("Unit1"),TestDevice("Unit2")]
    @Published var pSavedDevices = [DeviceTypeControl]()
    @Published var updatableFirmwareVersion: String?

    @Published var dfuPart: Int?
    @Published var dfuTotalParts: Int?
    @Published var dfuProgress: Int?
//    @Published var dfuState: HbtzDFUState?
//    @Published var dfuError: HbtzDFUError?
    @Published var dfuError: Int?
    @Published var dfuErrorMessage: String?

    var savedDevices: [DeviceTypeControl]
    {
        get {
        let controllers = controllers.filter({ $0.personalDeviceSerialNumber != nil })
        //print("Controllers: \(controllers)")
        return controllers
        }
    }
//    var availableDevices: [BleDiscoveryDevice]
//    {
//        get {
//            for controller in controllers {
//                print("TestController: \(controller.type) \(controller.advertisingDevice?.peripheral.name ?? "None") \(controller.advertisingDevice?.deviceSerialNumber.uint32 ?? 0) \(controller.personalDeviceSerialNumber ?? 0)")
//            }
//            let devices = controllers.filter({ $0.advertisingDevice != nil && $0.advertisingDevice?.deviceSerialNumber.uint32 == $0.personalDeviceSerialNumber }).compactMap({ $0.advertisingDevice! })
//            print("Available Devices:\(devices)")
//            return devices
//        }
//    }
    var availableControllers: [DeviceTypeControl]
    {
        get {
//            for controller in controllers {
                //print("TestController: \(controller.type) \(controller.advertisingDevice?.peripheral.name ?? "None") \(controller.advertisingDevice?.deviceSerialNumber.uint32 ?? 0) \(controller.personalDeviceSerialNumber ?? 0)")
//            }
            let controllers = controllers.filter({ $0.hbtzDevice != nil || ($0.advertisingDevice != nil && $0.advertisingDevice?.deviceSerialNumber.uint32 == $0.personalDeviceSerialNumber) })
            //print("❄️Available Controllers:\(controllers)")
            return controllers
        }
    }

    @Published var pAvailableControllers: [DeviceTypeControl]?
    @Published var pConnectedController: DeviceTypeControl?
//    @Published var pConnectingController: DeviceTypeControl?

    var connectedController: DeviceTypeControl? {
        get {
            if let controller = controllers.filter({ $0.hbtzDevice != nil && $0.isPersonalDevice == true}).first {
                return controller
            }
            return nil
        }
    }
    
//    var connectingController: DeviceTypeControl? {
//        get {
//            if let controller = controllers.filter({ $0.isConnectin == true && $0.isPersonalDevice == true}).first {
//                return controller
//            }
//            return nil
//        }
//    }
    
    func updatePPS()
    {
        var staleDevices = Set<AntDevice>()
        var staleHbtzDevices = Set<HbtzDevice>()

        for device in antDevices {
            device.calcPps()
            let delta = device.lastSeen.timeIntervalSince(Date())
            if (device.pps < 0.1) && (delta < -5) {
                staleDevices.insert(device)
            }
        }
        for device in staleDevices {
            antDevices.remove(device)
        }
        for device in hbtzDevices {
            device.calcPps()
            let delta = device.lastSeen.timeIntervalSince(Date())
            if (device.pps < 0.1) && (delta < -5) {
                staleHbtzDevices.insert(device)
            }
        }
        for device in staleHbtzDevices {
            hbtzDevices.remove(device)
        }
    }

    func updateControllers()
    {
        pConnectedController = connectedController
        if availableControllers.count > 0 {
            pAvailableControllers = availableControllers
        } else {
            pAvailableControllers = nil
        }
        
        if savedDevices.count > 0 {
            pSavedDevices = savedDevices
        } else {
            pSavedDevices = [DeviceTypeControl]()
        }
    }
    
    func updateStrongestDevice() {
//        for controller in controllers {
//            print("Controller:\(controller.type) enabled:\(controller.isEnabled)  Adv:\(controller.advertisingDevice)")
//        }
        let ctrls = controllers.filter({ ($0.isEnabled == true) && $0.advertisingDevices.count != 0 }).map({ $0.advertisingDevices })
        
        var devices = [BleDiscoveryDevice]()
        for controller in ctrls {
            devices.append(contentsOf: controller.values)
        }
        //print("Devices:\(devices.map({ $0.peripheral.name}))")
        let sortedDevcies = devices.sorted(by: { $0.deviceRssi.avgRssi > $1.deviceRssi.avgRssi })
        //print("SortedDevices:\(sortedDevcies.map({ $0.peripheral.name}))")
        self.strongestRssi = sortedDevcies.first
        //print("StrongestRSSI: \(self.strongestRssi?.peripheral.name ?? "Unknown") \(self.strongestRssi?.deviceRssi.rssi ?? -128)")
    }
    
    func forgetDevice(_ device:BleDiscoveryDevice)
    {
        if let controller = controllers.filter({ $0.hbtzDevice?.serialNumber ?? 0 == device.deviceSerialNumber.uint32 }).first {
            self.forgetDevice(controller)
        }
    }
    
    func forgetDevice(_ device:BleDevice)
    {
        if let controller = controllers.filter({ $0.hbtzDevice?.serialNumber ?? 0 == device.serialNumber }).first {
            self.forgetDevice(controller)
        }
    }
    
    func forgetDevice(_ controller:DeviceTypeControl?)
    {
        if let controller = controller {
            if controller.type == .heartbeatz {
                _ = hbtzManager.setPreferredDevice(device: nil)
            }
            controller.forget()
            controller.disconnect()
            updateControllers()
        }
    }

    
    func disconnectDevice(_ device:BleDevice)
    {
        if let controller = controllers.filter({ $0.hbtzDevice == device }).first {
            controller.disconnect()
        }
    }
    
    func enableHbtz()
    {
        // Get the integrator key from North Pole Engineering
        
        //let infoDictionary: [String: Any] = Bundle.main.infoDictionary!
        if let infoDictionary: [String: Any] = Bundle.main.infoDictionary,
           let mySecretApiKey: String = infoDictionary["MySecretApiKey"] as? String {
            let result = self.hbtzManager.initManager(scanForTypes: [], integratorKey: mySecretApiKey) {
                print("❄️heartbeatz Manager Init Completed")
                self.isHbtzEnabled = true
                self.proximityConnect = true
            }
            
            if result == .success {
            } else {
                print("❄️heartbeatz Manager Initialization Error:\(result.description)")
            }
        } else {
            assertionFailure("FAILURE: Missing MY_SECRET_KEY in Config.xcconfig file. Refer to documentation for more details.")
        }
    }

    private var minProxRssi: Int8 = -55
    
    private override init()
    {
        self.ppsTimer = Timer()
        super.init()
        
        self.ppsTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [self] _ in
            //print("Timer Fired!!!")
            updatePPS()
        }

        
        controllers = [wlCtrl,hbtzUsbLegacyCtrl,hbtzUsbCtrl,hbtzHubbCtrl,hbtzHornetCtrl,groupCtrl,runnCtrl,wyûrCtrl,côrdCtrl,gemCtrl,broadcastCtrl]
#if os(watchOS)
        NotificationCenter.default.addObserver(forName: WKApplication.willEnterForegroundNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationWillEnterForegroundNotification!")
            //                DispatchQueue.main.asyncAfter(deadline: .now() + 11.0) {
            //                    self.enterActiveTest()
            //                }
        }
        NotificationCenter.default.addObserver(forName: WKApplication.didBecomeActiveNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationDidBecomeActiveNotification!")
            // This is the one that we use to wake up the screen
            //                DispatchQueue.main.asyncAfter(deadline: .now() + 11.0) {
            //                    self.enterActiveTest()
            //                }
        }
        NotificationCenter.default.addObserver(forName: WKApplication.didEnterBackgroundNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationDidEnterBackgroundNotification!")
            // Returned to watch face
            //self.enterBackground()
        }
        NotificationCenter.default.addObserver(forName: WKApplication.didFinishLaunchingNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationDidFinishLaunchingNotification!")
            //                DispatchQueue.main.asyncAfter(deadline: .now() + 11.0) {
            //                self.enterActiveTest()
            //                }
        }
        NotificationCenter.default.addObserver(forName: WKApplication.willResignActiveNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationWillResignActiveNotification!")
            // Screen has dimmed
            //self.enterInactive()
        }
        
        NotificationCenter.default.addObserver(forName: .dfuDeviceDisconnected, object: nil, queue: OperationQueue.main) { _ in
            //self.initObserver()
            HbtzBleManager.sharedHbtzBleManager.startScanning("Init Scanning")
        }
#endif
#if os(iOS)
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationWillEnterForegroundNotification!")
            //                self.enterActiveTest()
        }
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationDidBecomeActiveNotification!")
            // This is the one that we use to wake up the screen
            //                self.enterActiveTest()
        }
        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationDidEnterBackgroundNotification!")
            // Returned to watch face
            self.enterBackground()
        }
        NotificationCenter.default.addObserver(forName: UIApplication.didFinishLaunchingNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationDidFinishLaunchingNotification!")
            //                self.enterActiveTest()
        }
        NotificationCenter.default.addObserver(forName: UIApplication.willResignActiveNotification, object: nil, queue: OperationQueue.main) { _ in
            //print("Notification applicationWillResignActiveNotification!")
            // Screen has dimmed
            //                self.enterInactive()
        }
        
        NotificationCenter.default.addObserver(forName: .dfuDeviceDisconnected, object: nil, queue: OperationQueue.main) { _ in
            //self.initObserver()
            self.hbtzManager.startScanning("Init Scan")
        }
#endif
    }
    
    
    public func enterInactive() {
        self.watchFaceDimmedWorkoutActive = true
    }
    
    public func enterActive() {
        self.watchFaceDimmedWorkoutActive = false
    }
    
    public func enterActiveTest() {
        self.watchFaceDimmedWorkoutActive = false
        hbtzManager.startScanning("Scan when watch activated")
    }
    
    public func enterBackground() {
        self.watchFaceDimmedWorkoutActive = true
    }
    
    func connectDfu() {
        // disabled DFU in framework
    }
    
    
    func updateDeviceScanTypes()
    {
        var types = Set<HeartbeatzProductType>()
        
        if groupCtrl.isEnabled == true {
            types.insert(.heartbeatzClub)
        }
        if wlCtrl.isEnabled == true {
            types.insert(.heartbeatz)
        }
        if hbtzUsbLegacyCtrl.isEnabled == true {
            types.insert(.heartbeatzHbtzUSB)
        }
        if hbtzUsbCtrl.isEnabled == true {
            types.insert(.heartbeatzHbtzUSB)
        }
        if hbtzHubbCtrl.isEnabled == true {
            types.insert(.heartbeatzHbtzUSB)
        }
        if hbtzHornetCtrl.isEnabled == true {
            types.insert(.heartbeatzHbtzUSB)
        }
        if wyûrCtrl.isEnabled == true {
            types.insert(.heartbeatzWyûr)
        }
        if côrdCtrl.isEnabled == true {
            types.insert(.heartbeatzWyûrUSB)
        }
        if runnCtrl.isEnabled == true {
            types.insert(.heartbeatzRunn)
        }
        if gemCtrl.isEnabled == true {
            types.insert(.heartbeatzGem)
        }
        if broadcastCtrl.isEnabled == true {
            types.insert(.heartbeatzBcast)
        }

        hbtzManager.scanForDeviceTypes = Array(types)
    }
    
    func newAntID()
    {
        localAntId = UInt32.random(in: 1..<1048575)
    }
        
    func isBroacasting(value: Bool)
    {
        hbtzManager.isBroadcasting = value
    }
    
    func showRawBle(value: Bool)
    {
        hbtzManager.showRawAdvertisement = value
    }
    
    func setUsbMode(device: BleDevice, value: HeartbeatzUsbOperationalMode)
    {
        //let result:Bool = hbtzManager.setHbtzUsbOperationalMode(device: device, mode: value)
        let result = hbtzManager.setHbtzUsbOperationalMode(device: device, mode: value)

        if result == false {
            print("Operation Not Allowed")
        }
    }
}

extension ManagerDelegate: HbtzBleDelegate {
    func playHaptic(device: BleDevice, type: UInt8) {
        if self.useHaptic == .all || self.useHaptic == .warning {
            self.playHaptic(type: type)
        }
    }
    
    func alertMessageForDevice(device: BleDevice, msg: String?) {
#if !os(tvOS)
        if let msg = msg {
            if let controller = controllers.filter({ $0.hbtzDevice?.peripheral?.identifier == device.peripheral?.identifier }).first {
                controller.alertMsg = msg
            }
            let content = UNMutableNotificationContent()
            content.title = msg
            content.subtitle = "SUB"
            content.sound = UNNotificationSound.default
            content.categoryIdentifier = "myCategory"
            
            let category = UNNotificationCategory(identifier: "myCategory", actions: [], intentIdentifiers: [], options: [])
            UNUserNotificationCenter.current().setNotificationCategories([category])
            
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request) { (error) in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    print("Scheduled Successfully")
                }
            }
        }
#endif
    }
    
    func dfuDidUpdateState(device: BleDevice, state: HbtzDFUState) {
        print("❄️dfuDidUpdateState:\(state) \(device)")
        //print("❄️DFU DEVICE:\(device.peripheral?.identifier.description ?? "Unknown")")
        if let controller = controllers.filter({ $0.hbtzDevice?.peripheral?.identifier == device.peripheral?.identifier }).first {

            if controller.dfuState == .uploading && (state == .validating || state == .disconnecting) == false {
                controller.dfuState = nil
                print("❄️dfu aborting")
                return
            }
            controller.dfuState = state
            
            if state == .completed {
                controller.dfuState = nil
            }
        } else {
            print("Unknown DFU Device:\(device)")
        }
    }
    
    func deviceShouldValidate(device: BleDevice, vendorID: UInt16, locationID: UInt16) -> Bool {
        print("❄️deviceShouldValidate: true")
        return true
    }
    
    func dfuDidUpdateProgress(device: BleDevice, for part: Int, outOf totalParts: Int, to progress: Int, currentSpeedBytesPerSecond: Double, avgSpeedBytesPerSecond: Double) {
        
        print("❄️DFU Progress: \(progress)")

        dfuPart = part
        dfuTotalParts = totalParts
        dfuProgress = progress
        
        if dfuError != nil {
            dfuError = nil
            dfuErrorMessage = nil
        }
    }

    func dfuError(device: BleDevice, _ error: Int, didOccurWithMessage message: String) {
        print("❄️DFU Error\(error): \(message)")
        
        dfuErrorMessage = message
        dfuError = error
        
        if let controller = controllers.filter({ $0.hbtzDevice?.peripheral?.identifier == device.peripheral?.identifier }).first {

            controller.dfuState = .aborted
        } else {
            print("Unknown DFU Device:\(device)")
        }

    }

    func resettingConnectionProcess(device: BleDevice) {
        print("❄️Resetting Connection: \(device.peripheral?.name ?? "Unknown")")
    }
    
    func rawAdvertisementData(peripheral: CBPeripheral, advertisementData: [String : Any], rssi: NSNumber) {
        print("❄️Raw Data(\(rssi.intValue): \(advertisementData["kCBAdvDataManufacturerData"] ?? "")")
    }
    
    func CBError(_ error: Error) {
        print("❄️CBError:\(error.localizedDescription)")
    }
    
    func dfuPackageAvailable(device: BleDevice, firmwareVersions: [FirmwareVersion]) {
        for version in firmwareVersions {
            print("❄️New FW Available:\(device.peripheral?.name ?? "Unknown") - \(version.Version) - \(version.Description)")
        }
        if let controller = controllers.filter({ $0.pAdvertisingDevice?.serialNumber ?? 0 == device.serialNumber }).first {
            controller.availableFirmwares = firmwareVersions
        }
  }
    
    func deviceShouldEnableBleOutput(device: BleDevice) -> Bool {
        return true
    }
    
    func updatedScanningState(isScanning: Bool) {
        print("❄️Scanning: \(isScanning)")
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("❄️Central Updated: \(central.state.rawValue)")
        managerState = central.state
    }
    
    func deviceBatteryLevel(device: BleDevice, level: Measurement<UnitPercent>) {
        if HbtzBleManager.sharedHbtzBleManager.showDebugPrint == true {
            print("❄️Battery Level: \(level.value)\(level.unit.symbol)")
        }
    }
    
#if os(watchOS)
    func hkSessionState() -> HKWorkoutSessionState? {
        return hkManager.session?.state
    }
    
    func hkWorkoutConfiguration() -> HKWorkoutConfiguration? {
        return hkManager.session?.workoutConfiguration
    }
#endif
    
    func didReceiveMessage(message: String, key: Int, state: LongMessageState, error: LongMessageStateError?) {
        print(didReceiveMessage)
    }
    
    func didSendMessage(state: LongMessageState, error: LongMessageStateError?) {
        print("didSendMessage")
    }
    
    func hbtzIdString() -> String
    {
        #if os(iOS)
        return UIDevice.current.name
        #elseif os(watchOS)
        return WKInterfaceDevice.current().name
        #endif
    }
    
    func hostBatteryLevel() -> Float
    {
        var batteryLevel = Float(0.0)
        #if os(iOS)
        UIDevice.current.isBatteryMonitoringEnabled = true
        batteryLevel = UIDevice.current.batteryLevel
        #elseif os(watchOS)
        WKInterfaceDevice.current().isBatteryMonitoringEnabled = true
        batteryLevel = WKInterfaceDevice.current().batteryLevel
        #endif
        return batteryLevel
    }

    
    func valueForHeartRate(type: ElementType) -> Measurement<UnitCadence> {
        #if os(iOS)
        return currentHeartRate
        #elseif os(watchOS)
        return currentHeartRate
        #else
        return Measurement(value: 0, unit: .beatsPerMinute)
        #endif
    }
    
    func valueForCalories(type: ElementType) -> Measurement<UnitEnergy> {
        //print("valueForCalories")
        #if os(iOS)
        return Measurement(value: 40, unit: UnitEnergy.kilocalories)
        #elseif os(watchOS)
        return self.activeCalories
        #else
        return self.totalCalories
        #endif
    }
    
    func uniqueID() -> UInt32 {
        //print("uniqueID")
        return localAntId
    }
    
    func antID(device: BleDevice) -> UInt32? {
        //print("AntID")
        if controllers.filter({ $0.isEnabled == true && $0.isHornet == false }).first != nil {
            return localAntId
        }
        return nil
    }
    
    func didReceiveHornetMessage(device: BleDevice, message: Data) {
        //print("Hornet Msg:\(message.hexEncodedString(options: .upperCase))")
        guard message.count > 12 else {
            print("Message too small")
            return
        }
        let dataArray =  message.bytes
        let intArray = dataArray.map { Int8(bitPattern: $0) }
        let payload = message.subdata(in: 0..<8)
        let devType = AntDeviceType(rawValue: dataArray[10]) ?? .other
        let temp = UInt32(dataArray[11])
        let transType = UInt8(temp & 0x0F)
        let devId = UInt32(dataArray[8]) + (UInt32(dataArray[9])<<8) + ((temp>>4)<<16)
        let rssi = intArray[12]
        
        guard devType != .none else {
            return
        }
        if let antDevice = antDevices.filter( { $0.deviceType == devType && $0.deviceID == devId } ).first {
            antDevice.data = payload
            antDevice.rssi = rssi
        } else {
            let device = AntDevice(deviceID: devId, deviceType: devType, transType: transType, rssi: rssi, data: payload)
            antDevices.insert(device)
        }
    }

    func didReceiveHbtzRoutedMessage(device: BleDevice, serialNumber: UInt32,  message: Data) {
        //print("HbtzRouted Msg(\(serialNumber)):\(message.hexEncodedString(options: .upperCase))")

        if let hbtzDevice = hbtzDevices.filter( { $0.deviceID == serialNumber } ).first {
            hbtzDevice.data = message
            //antDevice.rssi = rssi
        } else {
            let device = HbtzDevice(deviceID: serialNumber,  data: message)
            hbtzDevices.insert(device)
        }
    }

    func playHaptic(type: UInt8) {
#if os(watchOS)
        if self.useHaptic == .all {
            switch type {
            case 0:
                let _ = WKInterfaceDevice.current().play(.start)
            case 1:
                let _ = WKInterfaceDevice.current().play(.retry)
            case 2:
                let _ = WKInterfaceDevice.current().play(.stop)
            default:
                let _ = WKInterfaceDevice.current().play(.notification)
            }
        }
#elseif os(iOS)
        if self.useHaptic == .all {
            let generator = UINotificationFeedbackGenerator()
            switch type {
            case 0:
                generator.notificationOccurred(.success)
            case 1:
                generator.notificationOccurred(.warning)
            case 2:
                generator.notificationOccurred(.error)
            default:
                generator.notificationOccurred(.error)
            }
        }
#endif
    }
    
    func bluetoothPermissionsAvailable(available: Bool) {
        print("❄️bluetoothPermissionsAvailable")
    }
    
    func deviceShouldConnect(device: BleDiscoveryDevice) -> Bool {
        let connectSuccess = true
        if let controller = controllers.filter({ $0.type == device.mfgType && ( $0.subType == .none || $0.subType == device.usbOperationalMode) && $0.isEnabled }).first {
            if controller.personalDeviceSerialNumber == nil || controller.personalDeviceSerialNumber == device.deviceSerialNumber.uint32 {
                controller.advertisingDevice = device
            }

            print("❄️deviceShouldConnect: \(device.mfgType) \(device.peripheral.name ?? "Undefined") \(device.deviceRssi.rssi) \(device.peripheral.state.rawValue)")
            if device.mfgType == .heartbeatzClub {
                //self.connectingDevice = device
                controller.isConnecting = true
                return connectSuccess
            }
            else if device.mfgType == .heartbeatzHbtzUSB && device.usbOperationalMode == .hubb && controller.isEnabled == true {
                controller.isConnecting = true
                return connectSuccess
            }
//            if device.mfgType == .heartbeatzHbtzUSB && device.usbOperationalMode == .hubb && controller.isHornet == true {
//                return connectSuccess
//            }

            updateStrongestDevice()
            if let advertisingDevice = controller.advertisingDevice {
                //if let selectedType = self.selectedType {
                    if advertisingDevice.mfgType == controller.type,
                       controller.isEnabled == true {
                        //controller.isEnabled == true, selectedType == controller.type {
                        // Update the advertisement data
                        //controller.advertisingDevice = device
                        if let personalDeviceSerialNumber = controller.personalDeviceSerialNumber {
                            print("❄️\(controller.type) Checking personalDeviceSerialNumber:\(personalDeviceSerialNumber) device.deviceSerialNumber:\(device.deviceSerialNumber.uint32)")
                            if personalDeviceSerialNumber == device.deviceSerialNumber.uint32 {
                                self.connectingController = controller
                                return connectSuccess
                            }
                        }
                    }
                //}
            }
            
            if device.deviceRssi.avgRssi > minProxRssi, self.proximityConnect == true {
                //print("Setting personalDeviceSerialNumber:\(wlUsbCtrl.personalDeviceSerialNumber ?? 0xFFFFFFFF) device.deviceSerialNumber:\(device.deviceSerialNumber.uint32)")
                //guard controller.hbtzDevice?.connectionState != .disconnected else { return false }
                self.connectingController = controller
                hbtzManager.stopScanning("Device Should Connect: Proximity")
                controller.isConnecting = true
                return connectSuccess
            }
        }
        
        
        
        
        
//        else if runnCtrl.isEnabled == true {
//            if let personalDeviceSerialNumber = runnCtrl.personalDeviceSerialNumber {
//                print("Runn personalDeviceSerialNumber:\(personalDeviceSerialNumber) device.deviceSerialNumber:\(device.deviceSerialNumber.uint32)")
//                if personalDeviceSerialNumber == device.deviceSerialNumber.uint32 {
//                    return true
//                }
//            }
//        }
//        else if côrdCtrl.isEnabled == true {
//            if let personalDeviceSerialNumber = côrdCtrl.personalDeviceSerialNumber {
//                print("CÔRD personalDeviceSerialNumber:\(personalDeviceSerialNumber) device.deviceSerialNumber:\(device.deviceSerialNumber.uint32)")
//                if personalDeviceSerialNumber == device.deviceSerialNumber.uint32 {
//                    return true
//                }
//            }
//        }
//        else if wyûrCtrl.isEnabled == true {
//            if let personalDeviceSerialNumber = wyûrCtrl.personalDeviceSerialNumber {
//                print("WYÛR personalDeviceSerialNumber:\(personalDeviceSerialNumber) device.deviceSerialNumber:\(device.deviceSerialNumber.uint32)")
//                if personalDeviceSerialNumber == device.deviceSerialNumber.uint32 {
//                    return true
//                }
//            }
//        }
//        else if gemCtrl.isEnabled == true {
//            if let personalDeviceSerialNumber = gemCtrl.personalDeviceSerialNumber {
//                print("GEM personalDeviceSerialNumber:\(personalDeviceSerialNumber) device.deviceSerialNumber:\(device.deviceSerialNumber.uint32)")
//                if personalDeviceSerialNumber == device.deviceSerialNumber.uint32 {
//                    return true
//                }
//            }
//        }

        //controller.isConnecting = false
        return false
    }
    
    func shouldRememberId(type: HeartbeatzProductType, subType: HeartbeatzUsbOperationalMode) -> Bool
    {
        switch(type) {
        case .heartbeatzClub: return false
        case .heartbeatzGem: return false
        case .heartbeatzHbtzUSB:
            if subType == .single {
                return true
            }
            return false
        default: return true
        }
    }
    
    func deviceDidConnect(device: BleDevice) {
        print("❄️deviceDidConnect: \(device.peripheral?.name ?? "Unknown") \(device.serialNumber)")
        
        let controller = controllers.filter( { $0.type == device.mfgDataType && ($0.subType == .none || $0.subType == device.hbtzUsbOperationalMode) && $0.isEnabled }).first
        
        if let controller = controller {
            controller.connected(device: device, rememberID: self.shouldRememberId(type: device.mfgDataType, subType: device.hbtzUsbOperationalMode))
            //controller.advertisingDevice = nil
            controller.isConnecting = false
            updateControllers()
        }
        
        self.warnForConnect(error: nil)

        
//        if device.mfgDataType == .heartbeatzHbtzUSB
//        {
//            wlUsbCtrl.connect(device: device, rememberID: true)
//        }
//        else if device.mfgDataType == .heartbeatz
//        {
//            wlCtrl.connect(device: device, rememberID: true)
//        }
//        else if device.mfgDataType == .heartbeatzRunn
//        {
//            runnCtrl.connect(device: device, rememberID: true)
//        }
//        else if device.mfgDataType == .heartbeatzWyûr
//        {
//            wyûrCtrl.connect(device: device, rememberID: true)
//        }
//        else if device.mfgDataType == .heartbeatzWyûrUSB
//        {
//            côrdCtrl.connect(device: device, rememberID: true)
//        }
//        else if device.mfgDataType == .heartbeatzGem
//        {
//            gemCtrl.connect(device: device)
//        }
//        else if device.mfgDataType == .heartbeatzClub
//        {
//            groupCtrl.connect(device: device)
//        }
        
        self.warnForConnect(error: nil)
    }
    
    func deviceDidDisconnect(device: BleDevice) {
        print("❄️deviceDidDisonnect: \(device.peripheral?.name ?? "Unknown") \(device.serialNumber)")
        self.connectingController = nil

        if device.mfgDataType == .heartbeatzHbtzUSB && device.hbtzUsbOperationalMode == .single {
            hbtzUsbCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatzHbtzUSB && device.hbtzUsbOperationalMode == .legacy {
            hbtzUsbLegacyCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatzHbtzUSB && device.hbtzUsbOperationalMode == .hubb {
            hbtzHubbCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatzHbtzUSB && device.hbtzUsbOperationalMode == .hubb {
            hbtzHornetCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatzHubb {
            hbtzHubbCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatz {
            wlCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatzClub {
            groupCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatzRunn {
            runnCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatzGem {
            gemCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatzWyûr {
            wyûrCtrl.disconnect()
        }
        else if device.mfgDataType == .heartbeatzWyûrUSB {
            côrdCtrl.disconnect()
        }

        self.warnForDisconnect(error: nil)
        //hbtzManager.startScanning()
    }
    
    func print_val(_ string: String) {
        if self.printValueUpdates == true {
            print(string)
        }
    }
    
    func updateValueForCadence(type: ElementType, value: Measurement<UnitCadence>) {
        print_val("❄️updateValueForCadence:\(type) Value:\(value)")
    }
    
    func updateValueForPower(type: ElementType, value: Measurement<UnitPower>) {
        print_val("❄️updateValueForPower\(type) Value:\(value)")
    }
    
    func updateValueForEnergy(type: ElementType, value: Measurement<UnitEnergy>) {
        print_val("❄️updateValueForEnergy\(type) Value:\(value)")
    }
    
    func updateValueForDuration(type: ElementType, value: Measurement<UnitDuration>) {
        print_val("❄️updateValueForDuration\(type) Value:\(value)")
    }
    
    func updateValueForSpeed(type: ElementType, value: Measurement<UnitSpeed>) {
        print_val("❄️updateValueForSpeed\(type) Value:\(value)")
    }
    
    func updateValueForLength(type: ElementType, value: Measurement<UnitLength>) {
        print_val("❄️updateValueForLength\(type) Value:\(value)")
    }
    
    func updateValueForCount(type: ElementType, value: Measurement<UnitCount>) {
        print_val("❄️updateValueForCount\(type) Value:\(value)")
    }
    
    func updateValueForPercent(type: ElementType, value: Measurement<UnitPercent>) {
        print_val("❄️updateValueForPercent\(type) Value:\(value)")
    }
    
    #if os(watchOS)
    func hkSessionState() -> HKWorkoutSessionState {
        print("❄️hkSessionState")
        return .notStarted
    }
    
    func hkActivityType() -> HKWorkoutActivityType {
        print("❄️hkActivityType")
        return .highIntensityIntervalTraining
    }
    #endif
    
    func pauseHkWorkout() {
        print("❄️pauseHkWorkout")
    }
    
    func resumeHkWorkout() {
        print("❄️resumeHkWorkout")
    }
    
    func requestStateChange(state: HeartbeatzSessionState) {
        print("❄️requestStateChange")
    }
    
    func supportedWorkoutTypes(types: [WorkoutType], recommended: WorkoutType) {
        print("❄️supportedWorkoutTypes")
    }
    
    func accumDataState(state: AccumulatedDataState) {
        print("❄️accumDataState")
    }
    
}

#if os(watchOS)
extension ManagerDelegate: HkManagerDelegate {
    func handleHeartRateUpdate(type: ElementType, value: Measurement<UnitCadence>) {
        if type == .heartrate {
            DispatchQueue.main.async {
                self.currentHeartRate = Measurement(value: value.value, unit: UnitCadence.beatsPerMinute)
                //print("Current HR(\(self)): \(self.currentHeartRate.value)")
            }
        }
        else if type == .avgHeartrate {
            print("❄️AvgHr: \(value.value)\(value.unit)")
        }
    }
    func handleActiveCalorieUpdate(type: ElementType, value: Measurement<UnitEnergy>) {
        if type == .activeCalories {
            DispatchQueue.main.async {
                self.activeCalories = Measurement(value: value.value, unit: UnitEnergy.kilocalories)
                //print("Current HR(\(self)): \(self.currentHeartRate.value)")
            }
        }
    }
    func handleTotalCalorieUpdate(type: ElementType, value: Measurement<UnitEnergy>) {
        if type == .totalCalories {
            DispatchQueue.main.async {
                self.totalCalories = Measurement(value: value.value, unit: UnitEnergy.kilocalories)
                //print("Current HR(\(self)): \(self.currentHeartRate.value)")
            }
        }
    }
}
#endif


//extension Data {
//    struct HexEncodingOptions: OptionSet {
//        public let rawValue: Int
//        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
//    }
//    
//    func hexEncodedString(options: HexEncodingOptions = []) -> String {
//        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
//        return self.map { String(format: format, $0) }.joined()
//    }
//}
extension Data {
    
//    var uint8: UInt8 {
//        get {
////            var number: UInt8 = 0
////            self.copyBytes(to:&number, count: MemoryLayout<UInt8>.size)
////            return number
//            let i8array = self.withUnsafeBytes { $0.load(as: UInt8.self) }
//            return i8array
//        }
//    }
//    var bytes: [UInt8] {
//            return [UInt8](self)
//    }
    

    var stringASCII: String? {
        get {
            return NSString(data: self, encoding: String.Encoding.ascii.rawValue) as String?
        }
    }
    
    var stringUTF8: String? {
        get {
            return NSString(data: self, encoding: String.Encoding.utf8.rawValue) as String?
        }
    }
}

extension ManagerDelegate {
    internal func warnForConnect(error: Error?){
#if os(watchOS)
        if self.useHaptic == .all || self.useHaptic == .warning {
            WKInterfaceDevice.current().play(.notification)
        }
#endif
    }

    internal func warnForDisconnect(error: Error?){
#if os(watchOS)
        if self.useHaptic == .all || self.useHaptic == .warning {
            WKInterfaceDevice.current().play(.failure)
        }
        #elseif os(iOS)
        if self.useHaptic == .all || self.useHaptic == .warning {
        let impactMed = UIImpactFeedbackGenerator(style: .medium)
            impactMed.impactOccurred()
        }
#endif
    }
}

