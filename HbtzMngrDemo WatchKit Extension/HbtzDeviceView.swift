//
//  ANTDataView.swift
//  HbtzMngrDemo
//
//  Created by Rick Gibbs on 5/26/22.
//

import SwiftUI
import Combine
import HbtzManager

//class TimerWrapper : ObservableObject {
//    let objectWillChange = ObservableObjectPublisher()
//
//    var timer : Timer!
//    func start(withTimeInterval interval: Double = 0.1) {
//        self.timer?.invalidate()
//        self.timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { _ in
//            self.objectWillChange.send()
//        }
//    }
//    
//    func stop() {
//        self.timer?.invalidate()
//    }
//}

struct HbtzDeviceView: View {
    @ObservedObject var manager = ManagerDelegate.sharedManager
    @Environment(\.colorScheme) var colorScheme

    var body: some View {
        ZStack{
            RadialGradient(gradient: Gradient(colors: colorScheme == .dark ? [Color(UIColor.darkGray), .black] : [Color(UIColor.lightGray), .white]), center: .center, startRadius: 2, endRadius: 650)
            VStack {
                Text("heartbeatz Devices").fontWeight(.bold).padding()
                Divider()
                ScrollView(.vertical) {
                    ForEach(manager.hbtzDevices.sorted(by: { $0.deviceID < $1.deviceID })) { device in
                        HbtzDeviceDetailView(device: device)
                        Divider()
                    }
                }
            }
        }
    }
}

//struct SectionView: View {
//    @ObservedObject var manager = ManagerDelegate.sharedManager
//
//    var group: AntDeviceType
//
//    var body: some View {
//        Section(header:
//                    VStack(alignment: .leading) {
//            HStack {
//                Text("\(group.description) (\(group.rawValue))").padding(.leading, 10).foregroundColor(.white)
//                Spacer()
//            }.background(Color.blue.edgesIgnoringSafeArea(.all)).cornerRadius(15).padding()
//
//        }
//        ) {
//
//        }
//    }
//}
struct HbtzDeviceDetailView: View {
    @ObservedObject var device: HbtzDevice
    @Environment(\.colorScheme) var colorScheme

    @State private var showingAlert = false

    var body: some View {
        VStack {
            HStack {
                VStack(alignment: .leading) {
                    Text("ID: \(String(format: "%u (0x%x",device.deviceID, device.deviceID)))")
                    Text("PPS: \(String(format: "%2.2f", device.pps))")
                }.padding(.leading, 15)
                Spacer()
                VStack {
                    if let hrValue = device.hrValue {
                        HStack {
                            Spacer()
                            Text("HR: \(hrValue)").padding(.trailing, 5)
                        }
                    }
                    if let name = device.deviceName {
                        HStack {
                            Spacer()
                            Text(name).padding(.trailing, 5)
                        }
                    }
                }
            }
        }
        .popover(isPresented: $showingAlert, attachmentAnchor: .point(.bottom), arrowEdge: .top) {
            VStack {
                Text("Select Operation").fontWeight(.heavy).foregroundColor((colorScheme == .dark) ? .white : .black)
                Divider()
                //            Alert(title: Text("Important message"), message: Text("Wear sunscreen"), dismissButton: .default(Text("Got it!")))
                Button("Alert 1") { _ = HbtzBleManager.sharedHbtzBleManager.sendAlertToDevice(serialNumber: device.deviceID, haptic: 1, msg: "Alert 1") }.padding(.bottom, 10).foregroundColor((colorScheme == .dark) ? .white : .black)
                Button("Alert 2") { _ = HbtzBleManager.sharedHbtzBleManager.sendAlertToDevice(serialNumber: device.deviceID, haptic: 2, msg: "Second Alert") }.padding(.bottom, 10).foregroundColor((colorScheme == .dark) ? .white : .black)
                Button("Start") { _ = HbtzBleManager.sharedHbtzBleManager.controlWorkout(serialNumber: device.deviceID, action: .running) }.padding(.bottom, 10).foregroundColor((colorScheme == .dark) ? .white : .black)
                Button("Stop") {  _ = HbtzBleManager.sharedHbtzBleManager.controlWorkout(serialNumber: device.deviceID, action: .stop) }.padding(.bottom, 10).foregroundColor((colorScheme == .dark) ? .white : .black)
                Button("Pause") {  _ = HbtzBleManager.sharedHbtzBleManager.controlWorkout(serialNumber: device.deviceID, action: .paused) }.padding(.bottom, 10).foregroundColor((colorScheme == .dark) ? .white : .black)
                //Button("Dismiss") { showingAlert = false }
            }.frame(width: 350, height: 300)
                .foregroundColor(.black)
                //.background(.blue)
                .cornerRadius(30)
        }
        .onTapGesture(count: 1) {
                    showingAlert = true
                }
//        .onTapGesture(count: 2) {
//                    _ = HbtzBleManager.sharedHbtzBleManager.sendAlertToDevice(serialNumber: device.deviceID, haptic: 2, msg: "Test 2")
//                }
//        .onLongPressGesture(minimumDuration: 2) {
//                    _ = HbtzBleManager.sharedHbtzBleManager.sendAlertToDevice(serialNumber: device.deviceID, haptic: 3, msg: "Test 3")
//                }
    }
}

struct HbtzDeviceView_Previews: PreviewProvider {
    static var previews: some View {
        HbtzCtrlView()
    }
}
