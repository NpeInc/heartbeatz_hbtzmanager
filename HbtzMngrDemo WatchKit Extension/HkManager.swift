//
//  HkManager.swift
//  DemoApp WatchKit Extension
//
//  Created by Rick Gibbs on 12/21/21.
//

import Foundation
import HealthKit
import HbtzManager   
#if os(watchOS)
import WatchKit
#endif

struct PayloadKey {
    static let timeStamp = "timeStamp"
    static let colorData = "colorData"
    static let isCurrentComplicationInfo = "isCurrentComplicationInfo"
    static let pingData = "pingData"
    static let hrData = "hrData"
    static let connectedDevices = "connectedDevics"
    static let powerData = "powerData"
    static let cadenceData = "cadenceData"
    static let speedData = "speedData"
    static let distanceData = "distanceData"
    static let workoutState = "workoutState"
    static let uniqueId = "uniqueId"
}

public protocol HkManagerDelegate: AnyObject
{
    func handleHeartRateUpdate(type: ElementType, value: Measurement<UnitCadence>)
    func handleActiveCalorieUpdate(type: ElementType, value: Measurement<UnitEnergy>)
    func handleTotalCalorieUpdate(type: ElementType, value: Measurement<UnitEnergy>)
}

class HkManager: NSObject {
    public static let sharedManager = HkManager()
    
    var delegate:HkManagerDelegate?
    
    var activeCalories = Measurement.init(value: 0.0, unit: UnitEnergy.kilocalories)
    {
        willSet {
            self.delegate?.handleActiveCalorieUpdate(type: .activeCalories, value: newValue )
        }
    }
    var totalCalories = Measurement.init(value: 0.0, unit: UnitEnergy.kilocalories)
    {
        willSet {
            self.delegate?.handleTotalCalorieUpdate(type: .totalCalories, value: newValue )
        }
    }
    var totalDistance:Measurement? = Measurement.init(value: 0.0, unit: UnitLength.meters)
    var avgSpeed:Measurement? = Measurement(value: 0.0, unit: UnitSpeed.metersPerSecond)
    var avgBikeSpeed:Measurement? = Measurement(value: 0.0, unit: UnitSpeed.metersPerSecond)
    var currAvgHr:Measurement = Measurement(value: 0.0, unit: UnitCadence.beatsPerMinute)
    var avgCadence:Measurement = Measurement(value:0.0, unit: UnitCadence.stepsPerMinute)
    var sessionTime:TimeInterval? = 0

    var watchFaceDimmedWorkoutActive = false
    var appBackgrounded = false
    let healthStore = HKHealthStore()
    let configuration = HKWorkoutConfiguration()
    var interfaceIndicatesIdle: Bool = false
    var disconnectTimer: Timer?
    var idleSession = false
    
    var pauseTimer: Timer?
    @Published var stateString = "Undefined"
    
#if !os(iOS)
    //var recoveredSession: HKWorkoutSession!
    var session: HKWorkoutSession?
    var builder: HKLiveWorkoutBuilder?
    @Published public var lastSession: HKWorkoutSession?
    
#endif
    var prevAccumTime: Date?
    
    public var pauseInterval:TimeInterval = 0.0
    public var timerDate: Date?
    
    var workoutRecordTimer = Timer()
    var backgroundTimer = Timer()
    var workoutAveragingTimer = Timer()
    var currWorkoutStart: Date?
    var phoneCurrHr: Measurement<UnitCadence>?
    {
        willSet {
            self.delegate?.handleHeartRateUpdate(type: .heartrate, value: newValue ?? Measurement(value: 0, unit: .beatsPerMinute) )
        }
    }
    var prevHrValue: Double = 0
    let heartRateUnit = HKUnit.count().unitDivided(by: HKUnit.minute())
    var previousDistance: Measurement<UnitLength>?
    var previousDistanceDate: Date?
    var previousSteps: Measurement<UnitCount>?
    var accumPower: Measurement<UnitPower> = Measurement(value: 0.0, unit: UnitPower.watts)
    var accumPowerTime: TimeInterval?
    var prevBikeDistance: Measurement<UnitLength>?
    var accumCadence: Double = 0
    var accumCadenceTime: TimeInterval?
    var iTotalDistance: Measurement<UnitLength>?
    var iTotalBikingDistance: Measurement<UnitLength>?
    var seq: UInt8 = 0
    
    //The key path for the sample’s average heart rate.
    let HKPredicateKeyPathAveragePower: String = "HKPredicateKeyPathAveragePower"
    
    var acquireStr: String = "."
    
    @Published var lastHrTS: Date
    
//    func initIdleWorkout()
//    {
//        print("InitIdleWorkout")
//        self.idleSession = true;
//        
//        print("Prepping to recover workout")
//        //var result = false
//        self.healthStore.recoverActiveWorkoutSession { (session, error) in
//            if error != nil {
//                print("Error Recovering Session:\(String(describing: error))")
//                //result = false
//            } else {
//                
//                if session != nil {
//                    //Recovered workout
//                } else {
//                    // Create Workout
//                }
//                
//                if let hkSession = session {
//                    //self.recoveredSession = hkSession
//                    //_ = self.restartWorkout()
//                    //NotificationCenter.default.post(name: .workoutRecoveredEvent, object: nil)
//                }
//                //result = true
//            }
//        }
//    }
    
    
        
    public func getTimerDate() -> Date
    {
        return Date(timeInterval: -(self.builder?.elapsedTime ?? 0.0), since:  Date())
    }
    
    func workoutBuilder(_ workoutBuilder: HKLiveWorkoutBuilder, didCollectDataOf collectedTypes: Set<HKSampleType>) {
        
        for type in collectedTypes {
            guard let quantityType = type as? HKQuantityType else {
                return // Nothing to do.
            }
            
            let statistics = workoutBuilder.statistics(for: quantityType)
            
            // Dispatch to main, because we are updating the interface.
            DispatchQueue.main.async { [self] in
                
                switch statistics?.quantityType {
                case HKQuantityType.quantityType(forIdentifier: .heartRate):
                    if self.session?.state == .notStarted {
                        resumeWorkout()
                    }
                    /// - Tag: SetLabel
                    if #available(watchOS 8, *) {
                        lastHrTS = Date.now
                    } else {
                        // Fallback on earlier versions
                        lastHrTS = Date()
                    }
                    let value = statistics?.mostRecentQuantity()?.doubleValue(for: self.heartRateUnit)
                    let rounded = self.workoutRecordTimer.isValid == true ? Double( round( 1 * value! ) / 1 ) : 0
                    self.phoneCurrHr = Measurement(value: Double(UInt8(rounded)), unit: .beatsPerMinute)
                    if self.watchFaceDimmedWorkoutActive == false {
                        let rounded = Double( round( 1 * value! ) / 1 )
                        self.phoneCurrHr = Measurement(value: Double(UInt8(rounded)), unit: .beatsPerMinute)
                        //print("HR:\(self.phoneCurrHr!)  \(Date())")
                        self.delegate?.handleHeartRateUpdate(type: .heartrate, value: self.phoneCurrHr ?? Measurement(value: 0.0, unit: .beatsPerMinute))
                    }
//
                    self.prevHrValue = value ?? 0.0
                    
                case HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning):
                    if self.watchFaceDimmedWorkoutActive == false {
                        let distanceUnit = HKUnit.meter()
                        if let value = statistics?.sumQuantity()?.doubleValue(for: distanceUnit) {
                            self.totalDistance = Measurement.init(value: value, unit: UnitLength.meters)// HKUnit.init(from: .meter)
                            
                            if let sessionTime = self.builder?.elapsedTime {
                                let adjustedTime = sessionTime // + self.startupTime
                                if let distance = self.totalDistance {
                                    self.avgSpeed = Measurement(value: distance.value/adjustedTime, unit: UnitSpeed.metersPerSecond)
                                }
                            }
                        }
                    }
                    
                case HKQuantityType.quantityType(forIdentifier: .distanceCycling):
                    if self.watchFaceDimmedWorkoutActive == false {
                        let distanceUnit = HKUnit.meter()
                        if let value = statistics?.sumQuantity()?.doubleValue(for: distanceUnit) {
                            self.totalDistance = Measurement.init(value: value, unit: UnitLength.meters)// HKUnit.init(from: .meter)
                            
                            if let sessionTime = self.builder?.elapsedTime {
                                let adjustedTime = sessionTime // + self.startupTime
                                if let distance = self.totalDistance {
                                    self.avgBikeSpeed = Measurement(value: distance.value/adjustedTime, unit: UnitSpeed.metersPerSecond)
                                }
                            }
                        }
                    }
                case HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned):
                    if self.watchFaceDimmedWorkoutActive == false {
                        let calorieUnit = HKUnit.kilocalorie()
                        if let value = statistics?.sumQuantity()?.doubleValue(for: calorieUnit) {
                            self.activeCalories = Measurement.init(value: value, unit: .kilocalories)
                            self.delegate?.handleActiveCalorieUpdate(type: .activeCalories, value: self.activeCalories )
                        }
                    }
                case HKQuantityType.quantityType(forIdentifier: .basalEnergyBurned):
                    if self.watchFaceDimmedWorkoutActive == false {
                        let calorieUnit = HKUnit.kilocalorie()
                        if let basalValue = statistics?.sumQuantity()?.doubleValue(for: calorieUnit) {
                            let actCal = self.activeCalories
                            self.totalCalories = Measurement.init(value: basalValue + actCal.converted(to: .kilocalories).value, unit: .kilocalories)
                            self.delegate?.handleActiveCalorieUpdate(type: .totalCalories, value: self.totalCalories )
                        }
                    }
                case .none:
                    print("NONE")
                case .some(_):
                    print("SOME")
                }
            }
        }
    }
    
    
    func saveRunWalkDistanceSample(distance: Measurement<UnitLength>, date: Date) {
        if let prevDist = self.previousDistance {
            if let prevDate = self.previousDistanceDate {
                
                if self.session?.state == .running {
                    let deltaDistance = distance.converted(to: .meters).value - prevDist.converted(to: .meters).value
                    //print("DeltaDistance:\(deltaDistance)   \(distance.converted(to: .meters).value)  \(prevDist.converted(to: .meters).value)")
                    
                    guard let distanceType = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning) else {
                        fatalError("Distance Type is no longer available in HealthKit")
                    }
                    
                    if deltaDistance < 1 {
                        return
                    }
                    
                    if let session = self.session {
                        if session.state != .running {
                            return
                        }
                    }
                    
                    let distanceSample = HKQuantitySample(type: distanceType,
                                                          quantity: HKQuantity(unit: HKUnit.meter(),
                                                                               doubleValue: deltaDistance),
                                                          start: prevDate,
                                                          end: date)
                    
                    //Save the same to HealthKit
                    self.builder?.add([distanceSample], completion: { (success, error) in
                        if let error = error {
                            print("Error Saving Distance Sample: \(error.localizedDescription)")
                        } else {
                            //print("Successfully saved Distance Sample")
                        }
                    })
                    
                    if let _iTotalDistance = iTotalDistance {
                        iTotalDistance = Measurement(value:_iTotalDistance.value + deltaDistance, unit: UnitLength.meters)
                    } else {
                        iTotalDistance = Measurement(value: 0.0, unit: UnitLength.meters)
                    }
                }
                
                self.previousDistance = distance
                self.previousDistanceDate = date
                
            }
        } else {
            self.previousDistance = distance
            self.previousDistanceDate = date
        }
    }
    
    func saveBikingDistanceSample(distance: Measurement<UnitLength>, date: Date) {
        if let prevDist = self.previousDistance {
            let deltaDistance = distance.value - prevDist.value
            
            guard let _ = HKQuantityType.quantityType(forIdentifier: .distanceCycling) else {
                fatalError("Distance Type is no longer available in HealthKit")
            }
            
            if deltaDistance < 0.1 {
                return
            }
            
            if let session = self.session {
                if session.state != .running {
                    return
                }
            }
                        
            if let _iTotalBikingDistance = iTotalBikingDistance {
                iTotalBikingDistance = Measurement(value:_iTotalBikingDistance.value + deltaDistance, unit: UnitLength.meters)
            } else {
                iTotalBikingDistance = Measurement(value: 0.0, unit: UnitLength.meters)
            }
            
            self.previousDistance = distance
            
        } else {
            self.previousDistance = distance
        }
    }
    
    
    func saveStepsSample(steps: Measurement<UnitCount>, date: Date) {
        if let prevSteps = self.previousSteps {
            let deltaSteps = steps.value - prevSteps.value
            
            guard let stepsType = HKQuantityType.quantityType(forIdentifier: .stepCount) else {
                fatalError("step Count Type is no longer available in HealthKit")
            }
            
            if deltaSteps < 1 {
                return
            }
            
            //Use the Count HKUnit to track the steps quantity
            let stepQuantity = HKQuantity(unit: HKUnit.count(),
                                          doubleValue: deltaSteps)
            
            let stepsSample = HKQuantitySample(type: stepsType,
                                               quantity: stepQuantity,
                                               start: date,
                                               end: date)
            
            //Save the same to HealthKit
            self.builder?.add([stepsSample], completion: { (success, error) in
                if let error = error {
                    print("Error Saving Steps Sample: \(error.localizedDescription)")
                } else {
                    //print("Successfully saved Distance Sample")
                }
            })
            
            self.previousSteps = steps
        } else {
            self.previousSteps = steps
        }
    }
    
    public func resumeWorkout() {
        if self.session?.state == .notStarted {
            self.session?.prepare()
        }
//        if self.session?.state != .running {
//            self.session?.resume()
//            //self.setActiveScene(.activeWorkoutView)
//#if !os(iOS)
//            WKInterfaceDevice.current().play(.start)
//#endif
//        }
    }
    
//    public func idleWorkout() {
//        if self.session?.state == .stopped {
//            self.endWorkout(saveWorkout: false)
//            _ = self.createIdleWorkout()
//        }
//        if self.session?.state == .notStarted {
//            self.session?.startActivity(with: Date())
//        }
//        if self.session?.state != .paused {
//            self.session?.pause()
//#if !os(iOS)
//            //WKInterfaceDevice.current().play(.stop)
//#endif
//        }
//        interfaceIndicatesIdle = true
//    }
    
    public func pauseWorkout() {
        if self.session?.state != .paused {
            self.session?.pause()
#if !os(iOS)
            WKInterfaceDevice.current().play(.stop)
#endif
        }
    }
    
    func stopWorkout() {
        if self.session?.state == .ended ||
            self.session?.state == .stopped {
        } else {
            if self.builder?.elapsedTime ?? 0.0 < 60.0 {
                // Do not save a workout if the duration is less than a minute
                self.endWorkout(saveWorkout: false)
#if !os(iOS)
                WKInterfaceDevice.current().play(.success)
#endif
            } else {
                self.endWorkout(saveWorkout: true)
#if !os(iOS)
                WKInterfaceDevice.current().play(.success)
#endif
            }
        }
    }
    
//    func restartWorkout() -> Bool {
//        /// - Tag: restartWorkout
//
//        self.session = self.recoveredSession
//        self.pauseInterval = 0.0
//
//        if session?.state == .running {
//
//            // Create the timer to record the workout data from the FTMS service if it is present
//            self.workoutRecordTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (Timer) in
//                // Record the workout data into the active session
//                if self.builder != nil {
//
//                    if self.watchFaceDimmedWorkoutActive == false {
//                        self.sessionTime = self.builder?.elapsedTime
//                    }
//                    //self.processFitnessData()
//                }
//            })
//        } else {
//            // Create the timer to record the workout data from the FTMS service if it is present
//            self.workoutRecordTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (Timer) in
//                // Record the workout data into the active session
//                self.session?.resume()
//                if self.builder != nil {
//                    if self.watchFaceDimmedWorkoutActive == false {
//                        self.sessionTime = self.builder?.elapsedTime
//                    }
//                    //self.processFitnessData()
//                }
//            })
//        }
//
//
//        // Setup session and builder.
//        self.builder = self.session?.associatedWorkoutBuilder()
//        self.builder?.delegate = self
//        self.session?.delegate = self
//
//
//        /// Set the workout builder's data source.
//        /// - Tag: SetDataSource
//        self.builder?.dataSource = HKLiveWorkoutDataSource(healthStore: self.healthStore,
//                                                           workoutConfiguration: self.configuration)
//
//
//        // Start the workout session and begin data collection.
//        /// - Tag: StartSession
//        if self.session?.state != .running {
//            self.session?.startActivity(with: Date())
//        }
//
//        self.builder?.beginCollection(withStart: Date()) { (success, error) in
//            //self.xSession?.start()
//        }
//
//        return true
//    }
        
    
    var demo: Int = 1

    func createIdleWorkout(recoveredSession: HKWorkoutSession?) -> Bool {
        print("Creating Idle Workout")
        /// - Tag: CreateWorkout
        // Compare the workout type selected against the available sensor
        // If the sensor is not available for the type of workout selected how can the metrics to be displayed be determined???
        self.resetMetrics()
        
        //self.workoutType = workoutType.activity
        self.configuration.activityType = .other
        
        // Create the timer to record the workout data from the BLE services if they are present
//        self.workoutRecordTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (Timer) in
//            // Record the workout data into the active session
//            //            if self.builder != nil {
//            //                self.processFitnessData()
//            //            }
//            self.delegate?.handleHeartRateUpdate(type: .heartrate, value: Measurement(value: Double(self.demo), unit: .beatsPerMinute))
//            self.demo += 1
//        })
        
        self.configuration.locationType = .indoor
        
        if recoveredSession == nil {
            do {
                print("Creating Workout")
                self.phoneCurrHr = Measurement(value: 0.0, unit: .beatsPerMinute)

                self.session = try HKWorkoutSession(healthStore: self.healthStore, configuration: configuration)
            } catch {
                print("Error Starting Idle Session")
                return false
            }
        }
        else
        {
            self.session = recoveredSession
        }
        
        
        // Setup session and builder.
        print("Setting up Session Delegate")
        self.builder = self.session?.associatedWorkoutBuilder()
        self.builder?.delegate = self
        self.session?.delegate = self
        //self.session?.delegate = self
        
        /// Set the workout builder's data source.
        /// - Tag: SetDataSource
        self.builder?.dataSource = HKLiveWorkoutDataSource(healthStore: self.healthStore,
                                                           workoutConfiguration: self.configuration)
        
        
        // Start the workout session and begin data collection.
        /// - Tag: StartSession
        ///
//        if self.session?.state == .paused {
//            
//        }
//        else if self.session?.state != .running {
//            self.session?.startActivity(with: Date())
//            self.session?.pause()
//        }
        
        if self.session?.state == .notStarted {
            self.session?.prepare()
        }
        
        
        accumPowerTime = 0.0;
        accumCadenceTime = 0.0;
        accumPower = Measurement(value: 0.0, unit: UnitPower.watts)
        accumCadence = 0;
        
        self.builder?.beginCollection(withStart: Date()) { (success, error) in
            print("Beginning Collection")
            if error != nil {
                print("Error2:\(String(describing: error?.localizedDescription))")
            }
            if success == true {
                print("Collection Started")
            } else {
                print("Collection Failed To Start")
            }
        }
        
        self.currWorkoutStart = Date()
        
        
        return true
    }
    
//    func createWorkout() -> Bool {
//        /// - Tag: CreateWorkout
//        // Compare the workout type selected against the available sensor
//        // If the sensor is not available for the type of workout selected how can the metrics to be displayed be determined???
//        self.resetMetrics()
//
//        self.configuration.activityType = .other
//
//        // Create the timer to record the workout data from the BLE services if they are present
//        self.workoutRecordTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (Timer) in
//            // Record the workout data into the active session
//            if self.builder != nil {
//            }
//        })
//
//        self.configuration.locationType = .indoor
//
//        if self.recoveredSession == nil {
//            do {
//                self.session = try HKWorkoutSession(healthStore: self.healthStore, configuration: configuration)
//            } catch {
//                return false
//            }
//        } else {
//            self.session = self.recoveredSession
//        }
//
//        // Setup session and builder.
//        self.builder = self.session?.associatedWorkoutBuilder()
//        //        builder?.delegate = sessionDelegate
//        self.builder?.delegate = self
//        self.session?.delegate = self
//
//        /// Set the workout builder's data source.
//        /// - Tag: SetDataSource
//        self.builder?.dataSource = HKLiveWorkoutDataSource(healthStore: self.healthStore,
//                                                           workoutConfiguration: self.configuration)
//
//
//        // Start the workout session and begin data collection.
//        /// - Tag: StartSession
//        if self.session?.state != .running {
//            self.session?.startActivity(with: Date())
//        }
//
//
//        //startupTime = 0.0
//        accumPowerTime = 0.0;
//        //        accumDistanceTime = 0.0;
//        accumCadenceTime = 0.0;
//        accumPower = Measurement(value: 0.0, unit: UnitPower.watts)
//        //accumDistance = Measurement(value: 0.0, unit: UnitLength.meters)
//        accumCadence = 0;
//
//        self.builder?.beginCollection(withStart: Date()) { (success, error) in
//            //self.xSession?.start()
//            if error != nil {
//                print("Error:\(error?.localizedDescription)")
//            }
//        }
//
//        self.currWorkoutStart = Date()
//
//
//        return true
//    }
    
    func locationString(type: HKWorkoutSessionLocationType?) -> String {
        if let localType = type {
            switch localType {
            case .indoor:
                return "Indoor"
            case .outdoor:
                return "Outdoor"
            default:
                return ""
            }
        } else {
            return ""
        }
    }
    
    func endWorkout(saveWorkout: Bool) {
                
        // Build the meta data
        var metaData: [String:Any] = [:]
        
        metaData[HKMetadataKeyWorkoutBrandName] = "workoutTitle"
        metaData[HKMetadataKeyDeviceManufacturerName] = "North Pole Engineering"
        metaData[HKMetadataKeyDeviceName] = "Heartbeatz"
        
        print("Session State:\(self.session?.state.description ?? "Unknown")")
        self.builder?.addMetadata(metaData, completion: { (success, error) in
            
        })
        if let session = self.session {
            if session.state == .running  {
                session.pause()
            }
        }
        
        
        self.phoneCurrHr = nil
        self.workoutRecordTimer.invalidate()
        
        if let localBuilder = self.builder {
            let time = Int(self.builder?.elapsedTime ?? 0)
            
            localBuilder.endCollection(withEnd: Date(), completion: { (success, error) in
                //handle succes and error
                if (error != nil) {
                    print("EndCollectionError:\(String(describing: error))")
                } else {
                    if time < 60 || saveWorkout == false {
                        self.cancelWorkout()
                    } else {
                        self.session?.stopActivity(with: Date())
                        //self.session?.end()
                        localBuilder.finishWorkout { (workout, error) in
                            if (error != nil) {
                                print("finishWorkoutError:\(String(describing: error))")
                            } else {
                                print("Workout:\(String(describing: workout))")
                                self.session?.end()
                                //self.session = nil
                                let tempSession = self.session
                                DispatchQueue.main.async {
                                    self.lastSession = tempSession
                                    //self.getActivityRings()
                                    self.session = nil
                                }
                            }
                        }
                    }
                }
                self.previousDistance = nil
                //self.recoveredSession = nil
                self.prevAccumTime = nil
                self.prevBikeDistance = nil
                self.accumPower = Measurement(value: 0.0, unit: UnitPower.watts)
                self.accumCadence = 0
                
                self.phoneCurrHr = Measurement(value: 0, unit: .beatsPerMinute)
                self.session = nil
                self.builder = nil
                //self.currAvgHr = Measurement(value: 0, unit: .beatsPerMinute)

            })
        } else {
            print("Builder is nil")
        }
        
        //        self.totalDistance = Measurement(value: 0, unit: .meters)
        //        if let device = SystemState.sharedState.connectedDevice {
        //            device.clearAccumlatedValues()
        //        }
        
    }
    
    public func cancelWorkout()
    {
        if let localBuilder = self.builder {
            localBuilder.discardWorkout()
            if let ses = self.session {
                ses.end()
                print("SessionState:\(ses.state.description)")
                self.session = nil
            }
        }
    }
    
    
    func resetMetrics() {
        sessionTime = nil
        avgSpeed = nil
        avgBikeSpeed = nil
        //instantPower: Measurement<UnitPower> = Measurement(value: 0, unit: UnitPower.watts)
        //instantSpeed: Measurement<UnitSpeed> = Measurement(value: 0, unit: UnitSpeed.metersPerSecond)
        //pedalBalance: Measurement<UnitPercent> = Measurement(value: 0, unit: UnitPercent.percent)
    }
    
    
    func disableDisconnectTimer() {
        if disconnectTimer != nil {
            disconnectTimer?.invalidate()
            disconnectTimer = nil
        }
    }
    
    //    @objc func fireTimer() {
    //        print("TimerFired")
    //    }
    
    public func sessionActive() -> Bool {
        if session?.state == .running && self.idleSession == false {
            return true
        }
        return false
    }
    
    
    
    
    
    private override init() {
        
        //let disconnect = UNNotificationAction(identifier: "Disconnect", title: "Disconnect", options: [])
        //let alert = UNNotificationAction(identifier: "HEARTBEATZ_ALERT", title: "Alert", options: [])
        //let clear = UNNotificationAction(identifier: "clear", title: "Clear", options: [])
        //let category : UNNotificationCategory = UNNotificationCategory.init(identifier: "DISCONNECTNOTIFICATION", actions: [disconnect], intentIdentifiers: [], options: [])
        //let alertCategory : UNNotificationCategory = UNNotificationCategory.init(identifier: "HEARTBEATZALERT", actions: [alert], intentIdentifiers: [], options: [])
        
        
        if #available(watchOS 8, *) {
            lastHrTS = Date.now
        } else {
            // Fallback on earlier versions
            lastHrTS = Date()
        }

        super.init()
        
        
        
        
#if !os(iOS)
        
        //        WKApplication.applicationDidBecomeActiveNotification
        //        WKApplication.applicationDidEnterBackgroundNotification
        //        WKApplication.applicationDidFinishLaunchingNotification
        //        WKApplication.applicationWillEnterForegroundNotification
        //        WKApplication.applicationWillResignActiveNotification
        
        NotificationCenter.default.addObserver(forName: WKApplication.willEnterForegroundNotification, object: nil, queue: OperationQueue.main) { _ in
            print("Notification applicationWillEnterForegroundNotification!")
            //self.enterActiveTest()
        }
        NotificationCenter.default.addObserver(forName: WKApplication.didBecomeActiveNotification, object: nil, queue: OperationQueue.main) { _ in
            print("Notification applicationDidBecomeActiveNotification!")
            // This is the one that we use to wake up the screen
            //self.enterActiveTest()
        }
        NotificationCenter.default.addObserver(forName: WKApplication.didEnterBackgroundNotification, object: nil, queue: OperationQueue.main) { _ in
            print("Notification applicationDidEnterBackgroundNotification!")
            // Returned to watch face
            //self.enterBackground()
        }
        NotificationCenter.default.addObserver(forName: WKApplication.didFinishLaunchingNotification, object: nil, queue: OperationQueue.main) { _ in
            print("Notification applicationDidFinishLaunchingNotification!")
//            HKHealthStore().recoverActiveWorkoutSession(completion: { session, _ in
//                guard let session = session else {
//                    return
//                }
//                let builder = session.associatedWorkoutBuilder()
//                builder.discardWorkout()
//                print("Discarding Workout")
//            })
            
            //self.enterActiveTest()
        }
        NotificationCenter.default.addObserver(forName: WKApplication.willResignActiveNotification, object: nil, queue: OperationQueue.main) { _ in
            print("Notification applicationWillResignActiveNotification!")
            // Screen has dimmed
            //self.enterInactive()
        }
        
#endif
        
        
    }
    
    
    private func buildWorkoutState(active: Bool) -> [String: Any] {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium
        let timeString = dateFormatter.string(from: Date())
        
        let activeState = active == true ? "active" : "inactive"
        
        return [PayloadKey.timeStamp: timeString, PayloadKey.workoutState: activeState]
    }
    
    
    
    
    func getAVGHeartRate() {
        
        if let startDate = self.currWorkoutStart {
            let typeHeart = HKQuantityType.quantityType(forIdentifier: .heartRate)
            let predicate: NSPredicate? = HKQuery.predicateForSamples(withStart: startDate, end: Date(), options: HKQueryOptions.strictEndDate)
            
            let squery = HKStatisticsQuery(quantityType: typeHeart!, quantitySamplePredicate: predicate, options: .discreteAverage, completionHandler: {(query: HKStatisticsQuery,result: HKStatistics?, error: Error?) -> Void in
                DispatchQueue.main.async(execute: {() -> Void in
                    let quantity: HKQuantity? = result?.averageQuantity()
                    if let beats = quantity?.doubleValue(for: HKUnit.count().unitDivided(by: HKUnit.minute())) {
                        self.currAvgHr = Measurement(value: beats, unit: .beatsPerMinute)
                    }
                })
            })
            self.healthStore.execute(squery)
        }
    }
    
    func getAvgCadence(steps: Double) {
        
        if let startDate = self.currWorkoutStart {
            let typeSteps = HKQuantityType.quantityType(forIdentifier: .stepCount)
            let predicate: NSPredicate? = HKQuery.predicateForSamples(withStart: startDate, end: Date(), options: HKQueryOptions.strictEndDate)
            
            let squery = HKStatisticsQuery(quantityType: typeSteps!, quantitySamplePredicate: predicate, options: .cumulativeSum, completionHandler: {(query: HKStatisticsQuery,result: HKStatistics?, error: Error?) -> Void in
                DispatchQueue.main.async(execute: {() -> Void in
                    self.avgCadence = Measurement(value:steps/Date().timeIntervalSince(startDate), unit: .stepsPerMinute)
                })
            })
            self.healthStore.execute(squery)
        }
    }
    
    
    func setupHealthKit() {
        guard
            let steps = HKObjectType.quantityType(forIdentifier: .stepCount),
            let distance = HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning),
            let distanceCycling = HKObjectType.quantityType(forIdentifier: .distanceCycling),
            let activeEnergy = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned),
            let basalEnergy = HKObjectType.quantityType(forIdentifier: .basalEnergyBurned),
            let heartRate = HKObjectType.quantityType(forIdentifier: .heartRate)
        else {
            return
        }
        
        /// Requesting authorization.
        /// - Tag: RequestAuthorization
        // The quantity type to write to the health store.
        let typesToShare: Set = [HKQuantityType.workoutType(),distance,distanceCycling,steps]
        
        // The quantity types to read from the health store.
        let typesToRead: Set = [
            heartRate,
            activeEnergy,
            basalEnergy,
            steps,
            HKObjectType.activitySummaryType()
        ]
        
        // Request authorization for those quantity types.
        self.healthStore.requestAuthorization(toShare: typesToShare, read: typesToRead) { (success, error) in
            // Handle error. No error handling in this sample project.
        }
    }
    
    
    
    
    func getActivityRings() {
        let calendar = Calendar.current
        let endDate = Date()
        
        let startDate = Calendar.current.startOfDay(for: endDate)
        
        let units: Set<Calendar.Component> = [.day, .month, .year, .era]
        
        var startDateComponents = calendar.dateComponents(units, from: startDate)
        startDateComponents.calendar = calendar
        
        var endDateComponents = calendar.dateComponents(units, from: endDate)
        endDateComponents.calendar = calendar
        
        // Create the predicate for the query
        let summariesWithinRange = HKQuery.predicate(forActivitySummariesBetweenStart: startDateComponents,
                                                     end: endDateComponents)
        
        
        let query = HKActivitySummaryQuery(predicate: summariesWithinRange) { (query, summariesOrNil, errorOrNil) -> Void in
            
        }
        self.healthStore.execute(query)
    }
    
    func hrData(_ value: UInt8) -> [String: Any] {
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .medium
        let timeString = dateFormatter.string(from: Date())
        
        return [PayloadKey.timeStamp: timeString, PayloadKey.hrData: value]
    }
    
    func powerData(_ value: UInt16) -> [String: Any] {
        return [PayloadKey.powerData: value]
    }
    
    func cadenceData(_ value: UInt16) -> [String: Any] {
        return [PayloadKey.cadenceData: value]
    }
    
    func speedData(_ value: Double) -> [String: Any] {
        return [PayloadKey.speedData: value]
    }
    
    func distanceData(_ value: Double) -> [String: Any] {
        return [PayloadKey.distanceData: value]
    }
    
    func uniqueIdData(_ value: UInt16) -> [String: Any] {
        return [PayloadKey.uniqueId: value]
    }
    
        
    func onError(err: Error?) {
        print("Error:\(String(describing: err))")
    }
    
    func log(msg: String) {
        print("Log:\(msg)")
    }
}

extension HkManager: HKLiveWorkoutBuilderDelegate {
    
    func workoutBuilderDidCollectEvent(_ workoutBuilder: HKLiveWorkoutBuilder) {

        //print("workoutBuilderDidCollectEvent")
        self.timerDate = Date(timeInterval: -(self.builder?.elapsedTime ?? 0.0), since:  Date())
        //NotificationCenter.default.post(name: .durationUpdateEvent, object: nil, userInfo: nil)
    }
}

// MARK: - HKWorkoutSessionDelegate
extension HkManager: HKWorkoutSessionDelegate {
    func workoutSession(
        _ workoutSession: HKWorkoutSession,
        didChangeTo toState: HKWorkoutSessionState,
        from fromState: HKWorkoutSessionState,
        date: Date
    ) {
        print("workoutSessionDidChangeTo:\(toState.description) From:\(fromState.description)")
        
        if toState == .prepared {
            self.session?.startActivity(with: Date())
        }
        if toState == .running {
            stateString = "Running"
            if #available(watchOS 8, *) {
                lastHrTS = Date.now
            } else {
                // Fallback on earlier versions
                lastHrTS = Date()
            }
//            self.session?.pause()
            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                self.session?.pause()
                
            }
        }
        if toState == .paused {
            stateString = "Paused"
            print("Paused")
            DispatchQueue.main.async {
                self.runTimer()
            }
        }
        
        
//        if toState != .notStarted {
//            self.isCreatingWorkout = false
//        }
        //        // Dispatch to main, because we are updating the interface.
        //        DispatchQueue.main.async {
        //            self.setupMenuItemsForWorkoutSessionState(toState)
        //        }
    }
    
    func runTimer() {
        pauseTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) {_ in
//            print("Timer Fired \(self.lastHrTS.timeIntervalSinceNow)")
            if self.lastHrTS.timeIntervalSinceNow < -7.0 {
                self.session?.resume()
                print("Resume")
            }
        }

    }
    
    func workoutSession(_ workoutSession: HKWorkoutSession, didFailWithError error: Error) {
        print("workoutSessionDidFail:\(error.localizedDescription)")
        // No error handling in this sample project.
    }
}

extension HKWorkoutSessionState {
    var description: String
    {
        switch self {
        case .notStarted: return "Not Started"
        case .running: return "Running"
        case .ended: return "Ended"
        case .paused: return "Paused"
        case .prepared: return "Prepared"
        case .stopped: return "Stopped"
        @unknown default: return "Fatal Error"
        }
    }
}

