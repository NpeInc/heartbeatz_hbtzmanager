//
//  HbtzCtrlView.swift
//  HbtzMngrDemo WatchKit Extension
//
//  Created by Rick Gibbs on 12/31/21.
//

import SwiftUI
import CoreBluetooth
import HbtzManager
import UserNotifications
//import Combine
//import Foundation
//import HeartbeatzBleFW

struct AdvertisingView: View {
    @State var ctrl: DeviceTypeControl
    @State var title: String

    @State var measuredRSSI = "127"
    @State var vendorID = "0"
    @State var locationID = "0"

    var body: some View {
        #if os(watchOS)
        #endif
        #if os(iOS)
        HStack {
            Spacer()
            Text(title)
            TextField(title, text: $locationID).background(RoundedRectangle(cornerRadius: 5).fill( Color(UIColor.lightGray))).foregroundColor(Color.white).padding(.horizontal)
            //Spacer()
            Button("Set") {
                print("Set \(title)")
                if let device = ctrl.hbtzDevice {
                    HbtzBleManager.sharedHbtzBleManager.setBleAdvertisingInfo(device: device, measuredRSSI: Int8(measuredRSSI)!, vendorID: UInt16(vendorID)!, locationID: UInt16(locationID)!)
                }
            }
            Spacer()
        }.onAppear() {
            if let ad = ctrl.advertisingDevice {
                measuredRSSI = "\(ad.measuredRssi ?? 127)"
                vendorID = "\(ad.vendorID ?? 0)"
                locationID = "\(ad.locationID ?? 0)"
            }

        }
        #endif
    }
}

struct DeviceTypeView: View {
    @ObservedObject var ctrl: DeviceTypeControl
    @ObservedObject var manager = ManagerDelegate.sharedManager
    @State private var isShowing = false
    @State private var editAdvertisement = false
    @State var bleName = ""
    @State private var operationalMode = HeartbeatzUsbOperationalMode.single

    @State var measuredRSSI = "-60"
    @State var vendorID = "0"
    @State var locationID = "0"

//    private enum Mode: String, Identifiable, CaseIterable {
//            case single, hubb, studio
//
//            var displayName: String { rawValue.capitalized }
//
//            var id: String { self.rawValue }
//        }


    var body: some View {        
        VStack {
            VStack {
                Toggle(isOn: $ctrl.isEnabled, label: {
                    Text("\(ctrl.title)").fontWeight(.bold)
                }).padding([.horizontal], 5.0)
                if ctrl.isConnecting == true {
                ProgressView().progressViewStyle(CircularProgressViewStyle()).disabled(ctrl.isConnecting)
                    .opacity(ctrl.isConnecting ? 1.0 : 0.0)
                }
            }
            if let device = ctrl.hbtzDevice, ctrl.hbtzDevice?.mfgDataType == ctrl.type {
                if device.connectionState == .connected {
                    VStack(alignment: .leading) {
                        HStack {
                            Text("BLE Name: ").fontWeight(.semibold)
                            Text("\(ctrl.hbtzDevice?.peripheral?.name ?? "")")
                            Spacer()
                        }
                        HStack {
                            Text("ID: ").fontWeight(.semibold)
                            Text("\(device.serialNumber.description)")
                            Spacer()
                        }
                        if let pdsn = ctrl.personalDeviceSerialNumber {
                            HStack {
                                Text("Pref: ").fontWeight(.semibold)
                                Text("\(pdsn.description)")
                                Spacer()
                            }
                        }
                        if let alertMsg = ctrl.alertMsg {
                            Text("Alert: \(alertMsg)")
                        }
                    }.padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 5))
                    HStack {
                        Button("Close")
                        {
                            print("Disconnect")
                            ctrl.disconnect()
                        }
                        .padding()
                        if ctrl.personalDeviceSerialNumber != nil {
                            Button("Forget")
                            {
                                print("Forget")
                                manager.forgetDevice(ctrl)
                            }
                            .padding()
                        }
                    }
                    if ctrl.type == HeartbeatzProductType.heartbeatzHbtzUSB || ctrl.type == HeartbeatzProductType.heartbeatzHubb {
                        VStack {
                            HStack {
                                //                            VStack {
                                //                                Text("Mode").padding(.bottom, 10)
                                //                                Text("\(operationalMode.displayName)")
                                //                            }
                                //                            Spacer()
                                Picker("Op Mode", selection: $operationalMode.onChange(usbModeChange), content: {
                                    Text(HeartbeatzUsbOperationalMode.single.displayName()).tag(HeartbeatzUsbOperationalMode.single)
                                    Text(HeartbeatzUsbOperationalMode.hubb.displayName()).tag(HeartbeatzUsbOperationalMode.hubb)
                                    //Text(HeartbeatzUsbOperationalMode.studio.displayName()).tag(HeartbeatzUsbOperationalMode.studio)
                                    //Text(HeartbeatzUsbOperationalMode.hornet.displayName()).tag(HeartbeatzUsbOperationalMode.hornet)
                                })
#if os(iOS)
                                .pickerStyle(.segmented)
#endif
                                .onAppear() {
                                    if let opMode = ctrl.advertisingDevice?.usbOperationalMode {
                                        operationalMode = opMode
                                    }
                                }
                                //.padding()
#if os(watchOS)
                                Button("Set")
                                {
                                    if let device = ctrl.hbtzDevice {
                                        manager.setUsbMode(device: device, value: operationalMode)
                                        self.ctrl.hbtzDevice?.connectionState = .disconnecting
                                        //self.ctrl.disconnect()
                                        
                                        //print("Set the mode:\(operationalMode.displayName())")
                                    }
                                }
#endif
                            }
#if os(iOS)
                            Text("Operational Mode").fontWeight(.semibold)
                            #else
                            Text("OpMode").fontWeight(.semibold)
                            #endif
                        }
#if os(iOS)
                            if ctrl.isHornet == true, let advDev = ctrl.advertisingDevice {
                                StreamingView(ctrl: ctrl, advDev: advDev)
                            }

#endif
                            //                            #if os(iOS)
                            //                            Spacer()
                            //                            #endif
                        
                        HStack {
                            Spacer()
                            Rectangle().fill(.clear).frame(width: 10, height: 10)
#if os(iOS)
                            let text = "Edit Advertisement"
                            #else
                            let text = "Edit Advert"
                            #endif
                            Toggle(text, isOn: $editAdvertisement.animation()).onChange(of: editAdvertisement) { _editAdvertisement in
                                bleName = ctrl.hbtzDevice?.peripheral?.name ?? ""
                                if let ad = ctrl.advertisingDevice {
                                    measuredRSSI = "\(ad.measuredRssi ?? 127)"
                                    vendorID = "\(ad.vendorID ?? 0)"
                                    locationID = "\(ad.locationID ?? 0)"
                                }
                            }
                            Spacer()
                        }
                        if editAdvertisement {
                            //                            AdvertisingView(ctrl: ctrl, title: "Advertising Name")
                            //                            AdvertisingView(ctrl: ctrl, title: "Measured RSSI")
                            //                            AdvertisingView(ctrl: ctrl, title: "Vendor ID")
                            //                            AdvertisingView(ctrl: ctrl, title: "Location ID")
#if os(watchOS)
                            VStack {
                                Divider()
                                Text("Advertising Name")
                                TextField("Advertising Name", text: $bleName ).background(RoundedRectangle(cornerRadius: 5).fill( Color(UIColor.lightGray))).foregroundColor(Color.white).padding(.horizontal)
                                Button("Set") {
                                    print("Set Adv Name:\(bleName)")
                                    if let device = ctrl.hbtzDevice {
                                        HbtzBleManager.sharedHbtzBleManager.setBleAdvertisingName(device: device, name: bleName)
                                        device.connectionState = .disconnecting
                                    }
                                }
                            }
                            VStack {
                                Divider()
                                Text("Measured RSSI")
                                TextField("Measured RSSI", text: $measuredRSSI).background(RoundedRectangle(cornerRadius: 5).fill( Color(UIColor.lightGray))).foregroundColor(Color.white).padding(.horizontal)
                                Button("Set") {
                                    print("Set Measured RSSI")
                                    if let device = ctrl.hbtzDevice {
                                        HbtzBleManager.sharedHbtzBleManager.setBleAdvertisingInfo(device: device, measuredRSSI: Int8(measuredRSSI)!, vendorID: UInt16(vendorID)!, locationID: UInt16(locationID)!)
                                        device.connectionState = .disconnecting
                                    }
                                }
                            }
                            VStack {
                                Divider()
                                Text("Vendor ID")
                                TextField("Vendor ID", text: $vendorID).background(RoundedRectangle(cornerRadius: 5).fill( Color(UIColor.lightGray))).foregroundColor(Color.white).padding(.horizontal)
                                Button("Set") {
                                    print("Set Vendor ID")
                                    if let device = ctrl.hbtzDevice {
                                        HbtzBleManager.sharedHbtzBleManager.setBleAdvertisingInfo(device: device, measuredRSSI: Int8(measuredRSSI)!, vendorID: UInt16(vendorID)!, locationID: UInt16(locationID)!)
                                        device.connectionState = .disconnecting
                                    }
                                }
                            }
                            VStack {
                                Divider()
                                Text("Location ID")
                                TextField("Location ID", text: $locationID).background(RoundedRectangle(cornerRadius: 5).fill( Color(UIColor.lightGray))).foregroundColor(Color.white).padding(.horizontal)
                                Button("Set") {
                                    print("Set Location ID")
                                    if let device = ctrl.hbtzDevice {
                                        HbtzBleManager.sharedHbtzBleManager.setBleAdvertisingInfo(device: device, measuredRSSI: Int8(measuredRSSI)!, vendorID: UInt16(vendorID)!, locationID: UInt16(locationID)!)
                                        device.connectionState = .disconnecting
                                    }
                                }
                            }
#endif
#if os(iOS)
                            HStack {
                                Spacer()
                                Text("AdvName")
                                TextField("Advertising Name", text: $bleName ).background(RoundedRectangle(cornerRadius: 5).fill( Color(UIColor.lightGray))).foregroundColor(Color.white).padding(.horizontal)
                                Spacer()
                                Button("Set") {
                                    print("Set Adv Name:\(bleName)")
                                    if let device = ctrl.hbtzDevice {
                                        HbtzBleManager.sharedHbtzBleManager.setBleAdvertisingName(device: device, name: bleName)
                                        device.connectionState = .disconnecting
                                    }
                                }
                                Spacer()
                            }
                            HStack {
                                Spacer()
                                Text("measRSSI ")
                                TextField("Measured RSSI", text: $measuredRSSI).background(RoundedRectangle(cornerRadius: 5).fill( Color(UIColor.lightGray))).foregroundColor(Color.white).padding(.horizontal)
                                Spacer()
                                Button("Set") {
                                    print("Set Measured RSSI")
                                    if let device = ctrl.hbtzDevice {
                                        HbtzBleManager.sharedHbtzBleManager.setBleAdvertisingInfo(device: device, measuredRSSI: Int8(measuredRSSI)!, vendorID: UInt16(vendorID)!, locationID: UInt16(locationID)!)
                                        device.connectionState = .disconnecting
                                    }
                                }
                                Spacer()
                            }
                            HStack {
                                Spacer()
                                Text("VendorID")
                                TextField("Vendor ID", text: $vendorID).background(RoundedRectangle(cornerRadius: 5).fill( Color(UIColor.lightGray))).foregroundColor(Color.white).padding(.horizontal)
                                Spacer()
                                Button("Set") {
                                    print("Set Vendor ID")
                                    if let device = ctrl.hbtzDevice {
                                        HbtzBleManager.sharedHbtzBleManager.setBleAdvertisingInfo(device: device, measuredRSSI: Int8(measuredRSSI)!, vendorID: UInt16(vendorID)!, locationID: UInt16(locationID)!)
                                        device.connectionState = .disconnecting
                                    }
                                }
                                Spacer()
                            }
                            HStack {
                                Spacer()
                                Text("LocatID")
                                TextField("Location ID", text: $locationID).background(RoundedRectangle(cornerRadius: 5).fill( Color(UIColor.lightGray))).foregroundColor(Color.white).padding(.horizontal)
                                //Spacer()
                                Button("Set") {
                                    print("Set Location ID")
                                    if let device = ctrl.hbtzDevice {
                                        HbtzBleManager.sharedHbtzBleManager.setBleAdvertisingInfo(device: device, measuredRSSI: Int8(measuredRSSI)!, vendorID: UInt16(vendorID)!, locationID: UInt16(locationID)!)
                                        device.connectionState = .disconnecting
                                    }
                                }
                                Spacer()
                            }
#endif
                        }
                    }
                    if let device = ctrl.hbtzDevice {
                        HStack {
#if os(watchOS)
                            Text("FW Ver:")
                            #else
                            Text("FW Version:")
                            #endif
                            Spacer()
                            Text(device.firmwareVersion ?? "??")
                        }
                    }
                    if ctrl.availableFirmwares.count > 0 {
                        Text("Available Firmware")
                        if let dfuState = ctrl.dfuState {
                            Text("DFU State: \(dfuState.description())")
                        } else {
                            ForEach(ctrl.availableFirmwares, id: \.self) { firmware in
                                Button(firmware.Version) {
                                    if let device = ctrl.hbtzDevice {
                                        HbtzBleManager.sharedHbtzBleManager.performDfu(device: device, firmwareVersion: firmware)
                                        print("Perform DFU:\(firmware)")
                                        //                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else if let dfuState = ctrl.dfuState {
                VStack(alignment: .center) {
                Text("DFU State: \(dfuState.description())")
                Text("[\(manager.dfuPart ?? 0) of \(manager.dfuTotalParts ?? 0)] \(manager.dfuProgress ?? 0)%")
                    if let dfuError = manager.dfuError {
                        Text("Error(\(dfuError)): \(manager.dfuErrorMessage ?? "")")
                    }
                }
            } else if let personal = ctrl.personalDeviceSerialNumber {
                VStack(alignment: .leading) {
                    HStack {
                        Text("Pref: ").fontWeight(.semibold)
                        Text("\(personal.description)")
                        Spacer()
                    }
                }.padding(.leading, 10)
            }
            Divider()
        }
        .padding(EdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 5))
    }
    
    
    func usbModeChange(_ tag: HeartbeatzUsbOperationalMode)
    {
        #if os(iOS)
        if let device = ctrl.hbtzDevice {
        manager.setUsbMode(device: device, value: tag)
            self.ctrl.personalDeviceSerialNumber = nil;
        //manager.forgetDevice(self.ctrl)        //    self.ctrl.disconnect()
        self.ctrl.hbtzDevice?.connectionState = .disconnecting

        }
        #endif
    }
}

extension Binding {
     func toUnwrapped<T>(defaultValue: T) -> Binding<T> where Value == Optional<T>  {
        Binding<T>(get: { self.wrappedValue ?? defaultValue }, set: { self.wrappedValue = $0 })
    }

    func onChange(_ handler: @escaping (Value) -> Void) -> Binding<Value> {
        return Binding(
            get: { self.wrappedValue },
            set: { selection in
                self.wrappedValue = selection
                handler(selection)
        })
    }
}

struct StreamingView: View {
    @ObservedObject var ctrl: DeviceTypeControl
    @ObservedObject var advDev: BleDiscoveryDevice
    
    //@State var actionText = "Request"
    
    var body: some View {
        HStack {
            Button("\(advDev.usbAntStreaming ? "Disable" : "Enable") ANT Streaming")
            {
                ctrl.antStreaming(enable: !advDev.usbAntStreaming)
            }.padding()
            Button("\(advDev.usbHbtzStreaming ? "Disable" : "Enable") HBTZ Streaming")
            {
                ctrl.hbtzStreaming(enable: !advDev.usbHbtzStreaming)
            }
            .padding()
        }
    }
    
}


struct HbtzCtrlView: View {
    @ObservedObject var manager = ManagerDelegate.sharedManager
    @Environment(\.colorScheme) var colorScheme

    @State var isBroadcasting = false
    @State var isRawBle = false
    
    var body: some View {
#if os(iOS)
        ZStack{
            RadialGradient(gradient: Gradient(colors: colorScheme == .dark ? [Color(UIColor.darkGray), .black] : [Color(UIColor.lightGray), .white]), center: .center, startRadius: 2, endRadius: 650)
            ControllerViews()
        }
        #else
        ControllerViews()
            .onAppear() {
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge]) { (success, error) in
                    if success{
                        print("Notifications All set")
                    } else if let error = error {
                        print("Notification Auth Error: \(error.localizedDescription)")
                    }
                }

            }
#endif
    }
}

struct ControllerViews: View {
    @ObservedObject var manager = ManagerDelegate.sharedManager
    @State var isBroadcasting = false
    @State var isRawBle = false
    
    var body: some View {
        ScrollView {
            VStack {
                Text("\(manager.currentHeartRate.value, specifier: "%.0f") BPM")
                    .font(.title).padding(.top, 10)
                
                //printv("SessionState:\(manager.hkManager.session) \(manager.hkManager.isCreatingWorkout)")
#if os(watchOS)
                if manager.hkManager.session == nil
                {
                    Button("Create Workout")
                    {
                        print("Create Workout")
                        _ = manager.hkManager.createIdleWorkout(recoveredSession: nil)
                    }
                    .padding()
                }
                else if manager.hkManager.session?.state == .notStarted
                {
                    Button("Creating Workout")
                    {
                    }
                    .padding()
                }
                else if manager.hkManager.session?.state == .paused || manager.hkManager.session?.state == .running
                {
                    Button("End Workout")
                    {
                        print("End Workout")
                        manager.hkManager.endWorkout(saveWorkout: false)
                    }
                    .padding()
                    //}
                }
                Text("HK State: \(manager.hkManager.stateString)")
                Divider()
#endif
                if manager.isHbtzEnabled == false {
                    Button("Enable Hbtz")
                    {
                        manager.enableHbtz()
                    }
                    //if manager.managerState != .poweredOn {
                    //}
                    Divider()
                    Text("Bluetooth State: \(manager.managerState.description)")
                } else {
                    Group {
                        Divider()
                        Text("Bluetooth State: \(manager.managerState.description)")
                        Divider()
                        Text("ANT ID:\(manager.localAntId)")
                        Divider()
                    }
                    Group {
                        DeviceTypeView(ctrl: manager.wlCtrl)
                        DeviceTypeView(ctrl: manager.hbtzUsbLegacyCtrl)
                        DeviceTypeView(ctrl: manager.hbtzUsbCtrl)
                        DeviceTypeView(ctrl: manager.hbtzHubbCtrl)
#if !os(watchOS)
                        DeviceTypeView(ctrl: manager.hbtzHornetCtrl)
#endif
                    }
                    Group {
                        DeviceTypeView(ctrl: manager.groupCtrl)
                        DeviceTypeView(ctrl: manager.runnCtrl)
                        DeviceTypeView(ctrl: manager.wyûrCtrl)
                        DeviceTypeView(ctrl: manager.côrdCtrl)
                        DeviceTypeView(ctrl: manager.gemCtrl)
                        DeviceTypeView(ctrl: manager.broadcastCtrl)
                    }
                    Spacer()
                    Toggle("Publish", isOn: $isBroadcasting)
                        .onChange(of: isBroadcasting) { _isBroadcasting in
                            manager.isBroacasting(value: _isBroadcasting)
                        }
                        .padding()
                    Divider()
                    
                    Toggle("raw BLE", isOn: $isRawBle)
                        .onChange(of: isRawBle) { _isRawBle in
                            manager.showRawBle(value: _isRawBle)
                        }
                        .padding()
                }
                Group {
                    if manager.isHbtzEnabled == true {
                        Divider()
                        Button("New ANT ID")
                        {
                            manager.newAntID()
                        }
                    }
                    Divider()
                    Text("\(self.appVersion())")
                    Text("FW Ver: \(manager.frameworkVersion)")
                }
            }
        }
    }
    
    func appVersion() -> String
    {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "App Version:\(version) build \(build)"
    }
    
    func printv( _ data : Any)-> EmptyView{
        print(data)
        return EmptyView()
    }
}

struct HbtzCtrlView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            HbtzCtrlView()
        }
    }
}

extension CBManagerState {
    var description: String
    {
        switch self {
        case .poweredOn: return "Powered On"
        case .poweredOff: return "Powered Off"
        case .resetting: return "Resetting"
        case .unauthorized: return "Not Authorized"
        case .unsupported: return "Not Supported"
        case .unknown: return "Unknown"
        @unknown default: return "Fatal Error"
        }
    }
}

