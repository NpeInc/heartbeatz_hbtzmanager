//
//  DeviceTypeControl.swift
//  HbtzMngrDemo
//
//  Created by Rick Gibbs on 1/29/22.
//

import Foundation
import HbtzManager


class DeviceTypeControl: ObservableObject, Identifiable {
    var title: String = "Unknown"
    var type: HeartbeatzProductType
    var subType: HeartbeatzUsbOperationalMode
    var advertisingDevices = [UInt:BleDiscoveryDevice]()
    @Published var pAdvertisingDevice: BleDiscoveryDevice?
    var availableFirmwares = [FirmwareVersion]()
    let isHornet: Bool
    @Published var alertMsg: String?
    @Published var owningAntStreaming: Bool
    @Published var owningHbtzStreaming: Bool
    @Published var dfuState: HbtzDFUState?
    {
        didSet {
            print("DFU STATE: \(dfuState?.description() ?? "Unknown")")
        }
    }
    var advertisingDevice: BleDiscoveryDevice?
    {
        willSet {
            if let device = newValue {
                
                if self.hbtzDevice != nil {
                    advertisingDevices[UInt(device.deviceSerialNumber.uint32)] = device
                }
                
                if self.advertisingDevice == nil {
                    self.advertisingDevice = device
                }
                else if let advDevice = self.advertisingDevice {
                    if device.deviceRssi.avgRssi > advDevice.deviceRssi.avgRssi {
                        self.advertisingDevice = device
                    }
                    else if device.lastSeen.timeIntervalSince(advDevice.lastSeen) > 2 {
                        self.advertisingDevice = device
                    }
                }
            }
        }
        didSet {
            pAdvertisingDevice = advertisingDevice
        }
    }
    var isPersonalDevice: Bool {
        get {
            return ManagerDelegate.sharedManager.shouldRememberId(type: self.type, subType: self.subType)
        }
    }
    @Published var isEnabled: Bool = false
    { didSet {
        ManagerDelegate.sharedManager.updateDeviceScanTypes()
        HbtzBleManager.sharedHbtzBleManager.startScanning("Device Type \(type)")

        if let device = hbtzDevice, oldValue == true {
            // Disconnect the connected device when disabling the device type
            if device.connectionState == .connected {
                self.disconnect()
                //HbtzBleManager.sharedHbtzBleManager.disconnect(device: device)
            }
            self.isConnecting = false
        }
    }}
    @Published var isConnecting: Bool = false
    @Published var hbtzDevice: BleDevice?
    @Published var personalDeviceSerialNumber: UInt32?
    {
        didSet {
            if let personal = self.personalDeviceSerialNumber {
                UserDefaults.standard.set(String(personal), forKey: String("\(self.type)\(self.subType)PreferredDevice"))
            }
        }
    }
    
    init(title: String, type: HeartbeatzProductType, subType: HeartbeatzUsbOperationalMode = .none,  isHornet: Bool = false) {
        self.title = title
        self.type = type
        self.subType = subType
        self.owningAntStreaming = false
        self.owningHbtzStreaming = false
        self.isHornet = isHornet
        print("Init:\(type)")
        
        if let personalString = UserDefaults.standard.string(forKey: String("\(self.type)\(self.subType)PreferredDevice")) {
            if let personal = UInt32(personalString) {
                print("Personal Device Found:\(title) \(personal)")
                self.personalDeviceSerialNumber = UInt32(personal)
            }
        }
    }
        
    func connected(device: BleDevice, rememberID: Bool = false)
    {
        if rememberID == true {
            //if device.mfgDataType == .heartbeatz && HbtzBleManager.sharedHbtzBleManager.preferredDevice != nil {
            switch device.mfgDataType {
            case .heartbeatzClub:
                break
            case .heartbeatzGem:
                break
            case .heartbeatzHbtzUSB:
                if device.hbtzUsbOperationalMode == .single {
                    self.personalDeviceSerialNumber = device.serialNumber
                }
            default:
                self.personalDeviceSerialNumber = device.serialNumber
                break
            }
            print("❄️didConnect personalDeviceSerialNumber:\(self.personalDeviceSerialNumber ?? 0xFFFFFFFF)  \(self.type)")
            if self.isPersonalDevice == true, let personal = self.personalDeviceSerialNumber {
                UserDefaults.standard.set(String(personal), forKey: String("\(self.type)\(self.subType)PreferredDevice"))
            }
            if let personalString = UserDefaults.standard.string(forKey: String("\(self.type)\(self.subType)PreferredDevice")) {
                print("❄️Saved Personal Device:\(UInt32(personalString) ?? 0)")
            }
            if device.mfgDataType == .heartbeatz {
                _ = HbtzBleManager.sharedHbtzBleManager.setPreferredDevice(device: device)
            }
        } else {
                UserDefaults.standard.removeObject(forKey: String("\(self.type)\(self.subType)PreferredDevice"))
        }
        self.hbtzDevice = device
        //self.isConnected = true
        if self.isEnabled == false {
            self.isEnabled = true
        }
        self.advertisingDevices.removeAll()
    }
    
    func disconnect()
    {
        if let device = hbtzDevice {
            HbtzBleManager.sharedHbtzBleManager.disconnect(device: device)
            self.hbtzDevice = nil
            self.dfuState = nil
            self.owningAntStreaming = false
            //self.isConnected = false
            self.isConnecting = false
        }
    }
    
    func forget()
    {
        self.personalDeviceSerialNumber = nil
        self.disconnect()
        UserDefaults.standard.removeObject(forKey: String("\(self.type)\(self.subType)PreferredDevice"))
        ManagerDelegate.sharedManager.updateControllers()
//        if self.hbtzDevice?.mfgDataType == .heartbeatz {
//            // Remove the rememberred device if told to forget
//            _ = HbtzBleManager.sharedHbtzBleManager.setPreferredDevice(device: nil)
//        }
    }
    
    func antStreaming(enable: Bool)
    {
        if let device = self.hbtzDevice {
            let response = HbtzBleManager.sharedHbtzBleManager.enableAntStreaming(device: device, enable: enable)
            if response == true {
                self.owningAntStreaming = enable
            }
        }
    }
    
    func hbtzStreaming(enable: Bool)
    {
        if let device = self.hbtzDevice {
            let response = HbtzBleManager.sharedHbtzBleManager.enableHbtzStreaming(device: device, enable: enable)
            if response == true {
                self.owningHbtzStreaming = enable
            }
        }
    }
    
    func performDfu(firmware: FirmwareVersion)
    {
        if let device = hbtzDevice {
            HbtzBleManager.sharedHbtzBleManager.performDfu(device: device, firmwareVersion: firmware)
        }
    }
}

