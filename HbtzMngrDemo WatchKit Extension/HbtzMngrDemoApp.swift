//
//  HbtzMngrDemoApp.swift
//  HbtzMngrDemo WatchKit Extension
//
//  Created by Rick Gibbs on 12/31/21.
//

import SwiftUI

@main
struct HbtzMngrDemoApp: App {
    @WKApplicationDelegateAdaptor(ExtensionDelegate.self) var delegate
    
    @SceneBuilder var body: some Scene {
        WindowGroup {
                NavigationView {
                    HbtzCtrlView()
                }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
