//
//  AppDelegate.swift
//  DemoApp WatchKit Extension
//
//  Created by Rick Gibbs on 12/20/21.
//

#if os(watchOS)
import WatchKit
#endif
import HbtzManager
import HealthKit

class ExtensionDelegate: NSObject, WKApplicationDelegate {
    
    public static let sharedExtension = ExtensionDelegate()
    
    let hbtzManager = HbtzBleManager.sharedHbtzBleManager
    let hkManager = HkManager.sharedManager
    
    func applicationDidFinishLaunching() {
        print("Extension Delegate Active")
        hbtzManager.delegate = ManagerDelegate.sharedManager
        hbtzManager.showCommsPrint = false
        hbtzManager.showDebugPrint = false
        hbtzManager.showDetailDebugPrint = false

        hkManager.setupHealthKit()
        hkManager.delegate = ManagerDelegate.sharedManager
        //hkManager.initIdleWorkout()
        
    }
}
