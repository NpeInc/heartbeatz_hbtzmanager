//
//  ANTDataView.swift
//  HbtzMngrDemo
//
//  Created by Rick Gibbs on 5/26/22.
//

import SwiftUI
import Combine

//class TimerWrapper : ObservableObject {
//    let objectWillChange = ObservableObjectPublisher()
//
//    var timer : Timer!
//    func start(withTimeInterval interval: Double = 0.1) {
//        self.timer?.invalidate()
//        self.timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true) { _ in
//            self.objectWillChange.send()
//        }
//    }
//    
//    func stop() {
//        self.timer?.invalidate()
//    }
//}

struct ANTDataView: View {
    @ObservedObject var manager = ManagerDelegate.sharedManager
    @Environment(\.colorScheme) var colorScheme

    var groupByType: [AntDeviceType] {
        var groups =  [AntDeviceType]()
        for device in manager.antDevices.sorted(by: <) {
            let group = device.deviceType
            if groups.contains(group) == false {
                groups.append(group)
            }
        }
        return groups
    }
        
    var body: some View {
        ZStack{
            RadialGradient(gradient: Gradient(colors: colorScheme == .dark ? [Color(UIColor.darkGray), .black] : [Color(UIColor.lightGray), .white]), center: .center, startRadius: 2, endRadius: 650)
        VStack {
            Text("ANT Devices").fontWeight(.bold).padding()
            Divider()
            ScrollView(.vertical) {
                    ForEach(groupByType.sorted(by: { $0.rawValue < $1.rawValue }), id: \.self) { group in
                        SectionView(group: group)
                    }
            }
        }
        }
    }
}

struct SectionView: View {
    @ObservedObject var manager = ManagerDelegate.sharedManager

    var group: AntDeviceType
    
    var body: some View {
        Section(header:
                    VStack(alignment: .leading) {
            HStack {
                Text("\(group.description) (\(group.rawValue))").padding(.leading, 10).foregroundColor(.white)
                Spacer()
            }.background(Color.blue.edgesIgnoringSafeArea(.all)).cornerRadius(15).padding()
            
        }
        ) {
            ForEach(manager.antDevices.filter({$0.deviceType.rawValue == group.rawValue}).sorted(by: { $0.deviceID < $1.deviceID })) { device in
                DeviceDetailView(device: device)
                Divider()
            }
        }
    }
}
struct DeviceDetailView: View {
    @ObservedObject var device: AntDevice
    
    var body: some View {
        VStack {
            HStack {
                Text("ID: \(device.deviceID) (\(String(format: "0x%x", device.deviceID)))").padding(.leading, 15)
                Spacer()
                Text("RSSI: \(device.rssi)").padding(.trailing, 10)
                Text("PPS: \(String(format: "%.2f", device.pps))").padding(.trailing, 10)
            }
            if device.deviceType == .heartrate {
                HStack {
                    Text("HR: \(device.hrValue)").padding(.leading, 20)
                    Spacer()
                }
            }
        }
    }
}

struct ANTDataView_Previews: PreviewProvider {
    static var previews: some View {
        ANTDataView()
    }
}
