//
//  HbtzDFUView.swift
//  HbtzMngrDemo WatchKit Extension
//
//  Created by Rick Gibbs on 12/31/21.
//

import SwiftUI
import CoreBluetooth
import HbtzManager
//import Combine
//import Foundation
//import HeartbeatzBleFW



struct HbtzDFUView: View {
    @ObservedObject var manager = ManagerDelegate.sharedManager

    
    var body: some View {
        ScrollView {
            VStack {
                Text("Firmware Update")
                    .font(.title)

                if let connectedDevice = manager.pConnectedController?.hbtzDevice {


                        if let fwVersion = connectedDevice.firmwareVersion {
                            Text("Current Ver: \(fwVersion)")

                        }

                        if let updatableFwVer = manager.updatableFirmwareVersion {
                            Text("Available Ver: \(updatableFwVer)")

                            Button("Perform DFU")
                            {
                                //manager.hbtzManager.performDfu(device: connectedDevice, firmwareVersion: "")
                            }

//                            if let dfuState = manager.dfuState {
//                                Spacer()
//                                Text("Firmware Update \(dfuState.description())")
//                            }

                            if let dfuProgress = manager.dfuProgress,
                               let dfuPart = manager.dfuPart,
                               let dfuParts = manager.dfuTotalParts
                            {
                                //ProgressView("Part \(dfuPart)/\(dfuParts)", value: dfuProgress, total: 100)
                                ProgressView("Part \(dfuPart)/\(dfuParts): \(dfuProgress)%", value: Double(dfuProgress), total: 100)
                                    .padding()

                            }

                        }

                }


                Group {
                    Divider()
                    Text("\(self.appVersion())")
                    Text("Framework: \(manager.frameworkVersion)")
                }
            }
        }
    }
    
    func appVersion() -> String
    {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        return "App Version:\(version) build \(build)"
    }
    
    func printv( _ data : Any)-> EmptyView{
        print(data)
        return EmptyView()
    }
}

struct HbtzDFUView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            HbtzDFUView()
        }
    }
}



