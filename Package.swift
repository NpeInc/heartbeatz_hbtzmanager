// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "heartbeatzManagerPackage",
    products: [
        .library(name: "heartbeatzManagerPackage", targets: ["heartbeatz_manager_framework"]),
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(
            name: "heartbeatz_manager_framework",
            path: "Sources/heartbeatzManagerPackage/HbtzManager.xcframework"
        ),
        .target(
            name: "heartbeatzManagerPackage",
            dependencies: ["heartbeatz_manager_framework"]
        ),
        .testTarget(
            name: "heartbeatzManagerPackageTests",
            dependencies: ["heartbeatzManagerPackage"]
        ),
    ]
)

