//
//  HbtzMngrDemoApp.swift
//  HbtzMngrDemo
//
//  Created by Rick Gibbs on 12/31/21.
//

import SwiftUI

@main
struct TestApp: App {
    @UIApplicationDelegateAdaptor(iPhoneExtensionDelegate.self) var appDelegate
    @ObservedObject var manager = ManagerDelegate.sharedManager

var body: some Scene {


        WindowGroup {
            TabView {
                iPhoneIntroView()
                HbtzCtrlView()
                ANTDataView()
                HbtzDeviceView()
                //HbtzDFUView()
            }
            .tabViewStyle(PageTabViewStyle())
        }
    }
}
