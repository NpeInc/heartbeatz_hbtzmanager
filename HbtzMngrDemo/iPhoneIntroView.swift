//
//  iPhoneIntroView.swift
//  DemoApp WatchKit Extension
//
//  Created by Rick Gibbs on 12/20/21.
//

import SwiftUI
import HbtzManager

struct iPhoneIntroView: View {
    var body: some View {
        ZStack{
            RadialGradient(gradient: Gradient(colors: [.blue, .black]), center: .center, startRadius: 2, endRadius: 650)
            VStack(alignment: .center){
                Text("Heartbeatz Manager Framework")
                    .font(.title)
                    .fontWeight(.heavy)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
                    .padding([.top], 60.0)
                    .padding([.leading, .trailing])
                Image("NPELogo")
                    .resizable()
                    .padding()
                    .frame(width: 150.0, height: 150.0)
                    .scaledToFit()
                Spacer()
                Text("iOS/watchOS\nDemo Integration App")
                    .font(.title3)
                    .multilineTextAlignment(.center)
                    .foregroundColor(.white)
                Spacer()
                Text("Refer To Watch App For Operations")
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding(.top)
                Spacer()
                Text("North Pole Engineering")
                    .foregroundColor(.white)
                Text("©2022, All Rights Reserved")
                    .font(.footnote)
                    .foregroundColor(.white)
                    .padding(.bottom, 40.0)
            }
        }.edgesIgnoringSafeArea(.vertical)
            .preferredColorScheme(.light) // black tint on status bar

    }
}

struct iPhonIntroView_Previews: PreviewProvider {
    static var previews: some View {
        iPhoneIntroView()
    }
}
