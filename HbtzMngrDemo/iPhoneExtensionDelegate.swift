//
//  AppDelegate.swift
//  DemoApp WatchKit Extension
//
//  Created by Rick Gibbs on 12/20/21.
//

import Foundation
import UIKit
import HbtzManager
//import HealthKit

class iPhoneExtensionDelegate: NSObject, UIApplicationDelegate {
    
    public static let sharedExtension = iPhoneExtensionDelegate()
    
    let hbtzManager = HbtzBleManager.sharedHbtzBleManager
    //let hkManager = HkManager.sharedManager
    //let firebaseManager = FirmwareManager.sharedManager

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        print("Extension Delegate Active")
        hbtzManager.delegate = ManagerDelegate.sharedManager
        hbtzManager.showCommsPrint = false
        hbtzManager.showDebugPrint = false
        hbtzManager.showDetailDebugPrint = false

        //firebaseManager.queryDatabase()

        return true
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        print("Entereing Foregrund")
    }
    
}
